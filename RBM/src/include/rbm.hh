/*
 * $File: rbm.hh
 * $Date: Tue Nov 12 08:41:20 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#pragma once

#include <vector>
#include <string>
#include <functional>

/*!
 * \brief train one layer of restricted Boltzmann machine
 */
class RBMTrainer {
	public:
		RBMTrainer(int nr_visible, int nr_hidden);
		RBMTrainer();
		~RBMTrainer();

		//! callback when one iteration is finished
		std::function<void(RBMTrainer&)> on_iter_done;

		typedef const uint8_t *sample_t;
		typedef uint8_t *mutable_sample_t;
		struct SamplePool {
			std::vector<sample_t> train, validation;
		};

		void train(const SamplePool &data, const char *fout);

		/*!
		 * \brief reconstruct, put result back in sample and return free energy
		 * \param prob raw probability value for each visible unit
		 */
		float reconstruct(mutable_sample_t sample, float *prob = nullptr);

		float get_free_energy(sample_t sample);

		void load(const char *fin);

		RBMTrainer(RBMTrainer&) = delete;
		RBMTrainer(RBMTrainer&&) = delete;

		RBMTrainer& operator = (RBMTrainer&) = delete;
		RBMTrainer& operator = (RBMTrainer&&) = delete;
	private:
		typedef float num_t;
		struct Node {
			int state;
			num_t bias,
				  tot_input,	//! input before taking sigmoid
				  prob;			//! probability (taking sigmoid of tot_input)
		};
		std::vector<Node> m_visible, m_hidden;

		/*!
		 * m_weight[i * m_hidden.size() + j]:
		 * weight between v[i] and h[j]
		 */
		std::vector<num_t> m_weight;


		void sample_h2v(); 

		void sample_v2h(); 

		/*!
		 * \brief contrastive divergence learning; m_visible.state must have been
		 * initialized
		 */
		void cd_learn(num_t learning_rate, int nr_step);

		void set_sample(sample_t sample);

		void save(const char *fout);

		num_t calc_free_energy();

		num_t calc_avg_free_energy(const std::vector<sample_t> sgrp) {
			double sum = 0;
			for (auto &i: sgrp) {
				set_sample(i);
				sum += calc_free_energy();
			}
			return sum / sgrp.size();
		}


		class SampleStateWorkerPool;
		SampleStateWorkerPool *m_sample_state;
		friend class SampleStateWorkerPool;
};

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

