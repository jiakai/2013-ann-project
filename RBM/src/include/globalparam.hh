/*
 * $File: globalparam.hh
 * $Date: Mon Nov 04 23:36:59 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#pragma once

constexpr double WEIGHT_INIT_VARIANCE = 1;
constexpr int STOKE_AREA_THRESH = 30;
constexpr int PATCH_WIDTH = 13, PATCH_HEIGHT = 17;

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

