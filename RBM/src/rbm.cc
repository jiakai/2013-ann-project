/*
 * $File: rbm.cc
 * $Date: Mon Nov 18 08:24:22 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "rbm.hh"
#include "utils.hh"
#include "globalparam.hh"
#include "math.hh"
#include "config.hh"

#include <cstdio>
#include <thread>
#include <forward_list>
#include <random>
#include <condition_variable>
#include <fstream>

static constexpr int FREE_ENERGY_CYCLE = 5;

class RBMTrainer::SampleStateWorkerPool {
	int m_batch_size;
	const std::vector<Node> *m_from;
	std::vector<Node> *m_to = nullptr;
	decltype(m_to->begin()) m_to_start,	//! current position of to
		m_to_finished_pos;

	//! set in destructor to inform workers to stop
	bool m_on_destroy = false;	

	std::forward_list<std::thread> m_worker;
	std::condition_variable
		m_cond_start_worker, m_cond_worker_finish;
	std::mutex m_mtx;

	int m_nr_worker, m_wstride_from, m_wstride_to;

	public:
		SampleStateWorkerPool(RBMTrainer *trainer) {
			using random_engine = std::mt19937_64;
			auto worker = [trainer, this](
					random_engine::result_type rnd_seed) {
				random_engine rnd(rnd_seed);
				double rnd_factor = 1.0 / (random_engine::max() + 1.0);
				auto work_one_task = [&]() {
					for (; ; ) {
						decltype(m_to_start) tgt, tgt_end;
						{
							LOCK_GUARD(m_mtx);
							tgt = m_to_start;
							tgt_end = min(
									m_to->end(), m_to_start + m_batch_size);
							m_to_start = tgt_end;
						}
						if (tgt == tgt_end)
							return;
						int wstride_from = m_wstride_from,
							wstride_to = m_wstride_to;
						auto weight_start = trainer->m_weight.data() +
							wstride_to * (tgt - m_to->begin());
						int tgt_size = tgt_end - tgt;
						for (; tgt < tgt_end;
								tgt ++, weight_start += wstride_to) {
							auto w = weight_start;
							double x = tgt->bias;
							for (auto &i: *m_from) {
								if (i.state)
									x += *w;
								w += wstride_from;
							}
							tgt->tot_input = x;
							dassert(is_good_r(x));
							auto prob = 1.0 / (1.0 + exp(-x));
							tgt->prob = prob;
							tgt->state = rnd() * rnd_factor <= prob;
						}
						{
							LOCK_GUARD(m_mtx);
							m_to_finished_pos += tgt_size;
						}
					}
				};
				for (; ; ) {
					{
						std::unique_lock<std::mutex> lk(m_mtx);
						if (!m_to || m_to_start == m_to->end()) {
							m_cond_start_worker.wait(lk);
							if (m_on_destroy)
								return;
						}
					}
					work_one_task();

					//! tell master one worker finished
					m_cond_worker_finish.notify_one();
				}
			};

			std::random_device rand_seed;
			m_nr_worker = get_nr_worker_thread();
			for (int i = 0; i < m_nr_worker; i ++)
				m_worker.emplace_front(worker, rand_seed());
			log_printf("nr_worker=%d", m_nr_worker);
		}

		/*!
		 * \brief sample a layer from the other
		 * \param wstride_from delta of indices in m_weight of
		 * consecutive node in from
		 */
		void sample_state(const std::vector<Node> &from,
				std::vector<Node> &to, int wstride_from, int wstride_to) {
			{
				LOCK_GUARD(m_mtx);
				for (auto &i: to)
					i.tot_input = numeric_max<double>();

				m_batch_size = to.size() / (m_nr_worker * 2);
				m_from = &from;
				m_to = &to;
				m_to_finished_pos = m_to_start = to.begin();
				m_wstride_from = wstride_from;
				m_wstride_to = wstride_to;
			}
			m_cond_start_worker.notify_all();
			{
				std::unique_lock<std::mutex> lk(m_mtx);
				m_cond_worker_finish.wait(lk, [this](){
					return m_to_finished_pos == m_to->end();
				});
			}

			for (auto &i: to)
				dassert(is_good_r(i.tot_input));
		}

		~SampleStateWorkerPool() {
			{
				LOCK_GUARD(m_mtx);
				m_on_destroy = true;
				m_cond_start_worker.notify_all();
			}
			for (auto &i: m_worker)
				i.join();
		}
};

void RBMTrainer::sample_h2v() {
	m_sample_state->sample_state(m_hidden, m_visible, 1, m_hidden.size());
}

void RBMTrainer::sample_v2h() {
	m_sample_state->sample_state(m_visible, m_hidden, m_hidden.size(), 1);
}

void RBMTrainer::cd_learn(num_t learning_rate, int nr_step) {
	auto v_orig = m_visible;
	sample_v2h();
	auto h_orig = m_hidden;
	for (int i = 0; i < nr_step; i ++) {
		sample_h2v();
		sample_v2h();
	}

	for (size_t i = 0; i < v_orig.size(); i ++)
		m_visible[i].bias += learning_rate * (
				v_orig[i].state - m_visible[i].prob);

	for (size_t i = 0; i < h_orig.size(); i ++)
		m_hidden[i].bias += learning_rate * (
				h_orig[i].state - m_hidden[i].prob);

	for (size_t i = 0; i < v_orig.size(); i ++) {
		auto pi = v_orig[i].state;
		auto pi_t = m_visible[i].prob;
		auto w = m_weight.data() + i * h_orig.size();
		for (size_t j = 0; j < h_orig.size(); j ++)
			w[j] += learning_rate * ((pi * h_orig[j].prob) -
					(pi_t * m_hidden[j].prob));
	}
}

RBMTrainer::num_t RBMTrainer::calc_free_energy() {
	sample_v2h();
	double rst = 0;
	for (auto &i: m_visible)
		if (i.state) {
			rst -= i.bias;
			dassert(is_good_r(i.bias));
			dassert(is_good_r(rst));
		}
	for (auto &i: m_hidden) {
		dassert(is_good_r(i.tot_input));
		rst -= log(1 + exp(i.tot_input));
		dassert(is_good_r(rst));
	}
	return rst;
}

void RBMTrainer::set_sample(sample_t sample) {
	for (size_t i = 0; i < m_visible.size(); i ++)
		m_visible[i].state = sample[i] != 0;
}

RBMTrainer::RBMTrainer(int nr_visible, int nr_hidden):
	m_visible(nr_visible), m_hidden(nr_hidden),
	m_weight(nr_visible * nr_hidden),
	m_sample_state(new SampleStateWorkerPool(this))
{
	std::mt19937 rnd;
	std::normal_distribution<> normal_dist(0, WEIGHT_INIT_VARIANCE);
	for (auto &i: m_visible)
		i.bias = normal_dist(rnd);
	for (auto &i: m_visible)
		i.bias = normal_dist(rnd);
	for (auto &i: m_weight)
		i = normal_dist(rnd);
}

RBMTrainer::RBMTrainer():
	m_sample_state(new SampleStateWorkerPool(this))
{
}

RBMTrainer::~RBMTrainer() {
	delete m_sample_state;
}

void RBMTrainer::train(const SamplePool &data, const char *fout) {

	for (int iter = 0; ; iter ++) {
		auto rate = Config::get("train")["learning_rate"].as_double(0.1);
		int cnt = 0;
		for (auto &i: data.train) {
			set_sample(i);
			cd_learn(rate, 1);
			printf("train %d/%d\r", cnt ++, int(data.train.size()));
			fflush(stdout);
		}
		printf("\n");

		if (!(iter % FREE_ENERGY_CYCLE)) {
			auto etrain = calc_avg_free_energy(data.train),
				 evldt = calc_avg_free_energy(data.validation);
			log_printf("iter #%d: learning_rate=%.3f free energy: train=%.5f vldt=%.5f",
					iter, rate, etrain, evldt);
		}
		save(ssprintf("%s-%d", fout, iter).c_str());
		if (on_iter_done)
			on_iter_done(*this);
	}
}

void RBMTrainer::save(const char *fout_path) {
	FILE *fout = fopen(fout_path, "w");
	dassert(fout);
	fprintf(fout, "%d %d\n", int(m_visible.size()), int(m_hidden.size()));
	for (auto &i: m_visible)
		fprintf(fout, "%.8e ", i.bias);
	fprintf(fout, "\n");
	for (auto &i: m_hidden)
		fprintf(fout, "%.8e ", i.bias);
	fprintf(fout, "\n");
	for (auto &i: m_weight)
		fprintf(fout, "%.8e ", i);
	fprintf(fout, "\n");
	fclose(fout);
}

void RBMTrainer::load(const char *fin_path) {
	std::ifstream fin(fin_path);
	int n, m;
	fin >> n >> m;
	m_visible.resize(n);
	m_hidden.resize(m);
	m_weight.resize(n * m);
	for (auto &i: m_visible)
		fin >> i.bias;
	for (auto &i: m_hidden)
		fin >> i.bias;
	for (auto &i: m_weight)
		fin >> i;
	dassert(fin.good());
}

float RBMTrainer::reconstruct(mutable_sample_t sample, float *prob) {
	set_sample(sample);
	auto rst = calc_free_energy();
	sample_h2v();
	for (size_t i = 0; i < m_visible.size(); i ++)
		sample[i] = m_visible[i].state ? 255 : 0;
	if (prob) {
		for (size_t i = 0; i < m_visible.size(); i ++)
			prob[i] = m_visible[i].prob;
	}
	return rst;
}

float RBMTrainer::get_free_energy(sample_t sample) {
	set_sample(sample);
	return calc_free_energy();
}

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

