/*
 * $File: main.cc
 * $Date: Mon Nov 18 21:35:10 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "globalparam.hh"
#include "sample.hh"
#include "rbm.hh"
#include "config.hh"
#include <cmath>

static const int LABEL_SIZE = 1, NR_HIDDEN = 250,
			 RBM_SAMPLE_SIZE = PATCH_WIDTH * PATCH_HEIGHT + LABEL_SIZE * 10,
			 NR_RAND_TEST = 100, ACCURATE_TEST_CYCLE = 5;

// rbm sample format:
// patch[height][width]
// label[digit][LABEL_SIZE]

static void set_sample_label(RBMTrainer::mutable_sample_t dest, int digit) {
	dest += PATCH_WIDTH * PATCH_HEIGHT;
	memset(dest, 0, LABEL_SIZE * 10);
	if (digit >= 0)
		memset(dest + digit * LABEL_SIZE, -1, LABEL_SIZE);
}

static std::shared_ptr<uint8_t> gen_rbm_sample(const std::vector<Sample> &orig,
		std::vector<RBMTrainer::sample_t> &output_sample) {
	output_sample.resize(orig.size());
	auto buf = create_auto_buf<uint8_t>(orig.size() * RBM_SAMPLE_SIZE);
	auto dest = buf.get();
	for (size_t i = 0; i < orig.size(); i ++) {
		output_sample[i] = dest;
		memcpy(dest, orig[i].data(), PATCH_WIDTH * PATCH_HEIGHT);
		set_sample_label(dest, orig[i].get_groundtruth());
		dest += RBM_SAMPLE_SIZE;
	}
	return buf;
}

static void display_sample(RBMTrainer::sample_t sample,
		bool show_dig = true, bool show_label = true) {

	if (show_dig) {
		for (int i = 0; i < PATCH_HEIGHT; i ++) {
			for (int j = 0; j < PATCH_WIDTH; j ++)
				printf("%d", sample[i * PATCH_WIDTH + j] ? 1 : 0);
			printf("\n");
		}
	}
	if (show_label) {
		sample += PATCH_WIDTH * PATCH_HEIGHT;
		for (int i = 0; i < LABEL_SIZE; i ++) {
			for (int j = 0; j < 10; j ++)
				printf("%d", sample[j * LABEL_SIZE + i] ? 1 : 0);
			printf("\n");
		}
	}
}

static int get_rbm_predict(RBMTrainer &trainer, const Sample &sample,
		bool print_log) {
	unsigned char rbm_sample[RBM_SAMPLE_SIZE];
	memcpy(rbm_sample, sample.data(), PATCH_WIDTH * PATCH_HEIGHT);
	double accum_prob[10] = {0};
	set_sample_label(rbm_sample, -1);
	float prob_all[RBM_SAMPLE_SIZE];

	if (print_log) {
		printf("get_rbm_predict: input:\n");
		display_sample(rbm_sample);
	}

	trainer.reconstruct(rbm_sample, prob_all);
	auto prob_start = prob_all + PATCH_WIDTH * PATCH_HEIGHT;
	for (int t = 0; t < 10; t ++)
		for (int l = 0; l < LABEL_SIZE; l ++)
			accum_prob[t] += log(*(prob_start ++));
	int max_pos = 0;
	auto max_prob = accum_prob[0];
	for (int i = 1; i < 10; i ++)
		if (accum_prob[i] > max_prob) {
			max_prob = accum_prob[i];
			max_pos = i;
		}
	if (print_log) {
		printf("reconstruct for %d: prob: ", sample.get_groundtruth());
		for(int i = 0; i < 10; i ++)
			printf("%.6lf ", accum_prob[i]);
		printf("\nresult: %d\n", max_pos);
		display_sample(rbm_sample);
	}
	return max_pos;
}

static void train(const char *config_fpath) {
	Config::set_fpath(config_fpath);
	SamplePool spool;
	spool.load_from_conf();
	for (auto p: {&spool.train, &spool.validation, &spool.test})
		for (auto &i: *p) {
			auto d = i.data();
			for (int j = 0; j < PATCH_WIDTH * PATCH_HEIGHT; j ++)
				dassert(d[j] == 0 || d[j] == 255);
		}

	RBMTrainer::SamplePool rbm_spool;
	auto train_buf = gen_rbm_sample(spool.train, rbm_spool.train),
		 vldt_buf = gen_rbm_sample(spool.validation, rbm_spool.validation);
	RBMTrainer trainer(RBM_SAMPLE_SIZE, NR_HIDDEN);
	{
		auto preload_fpath = Config::get("train")["preload"].as_string("");
		if (!preload_fpath.empty()) {
			trainer.load(preload_fpath.c_str());
			log_printf("preload from %s", preload_fpath.c_str());
		}
	}
	int nr_iter = 0;
	trainer.on_iter_done = [&](RBMTrainer &) {
		for (int i = 0; i < 20; i ++) {
			unsigned char tmp[RBM_SAMPLE_SIZE];
			memcpy(tmp,
					rbm_spool.validation[
					irand(0, rbm_spool.validation.size())],
					RBM_SAMPLE_SIZE);

			printf("orig:\n");
			display_sample(tmp);
			printf("reconstruct:\n");
			trainer.reconstruct(tmp);
			display_sample(tmp);
			printf("==============\n");
		}
		int nr_right = 0, nr_test;
		if (nr_iter % ACCURATE_TEST_CYCLE) {
			nr_test = NR_RAND_TEST;
			bool tested[10] = {0};
			for (int i = 0; i < NR_RAND_TEST; i ++) {
				auto &s = spool.test[irand(0, spool.test.size())];
				int t = get_rbm_predict(trainer, s, !tested[s.get_groundtruth()]);
				tested[s.get_groundtruth()] = true;
				if (t == s.get_groundtruth())
					nr_right ++;
			}
		} else {
			nr_test = spool.test.size();
			for (int i = 0; i < nr_test; i ++) {
				auto &s = spool.test[i];
				int t = get_rbm_predict(trainer, s, false);
				if (t == s.get_groundtruth())
					nr_right ++;
			}
		}
		log_printf("test: tp=%.3f%% (%d/%d)", double(nr_right) / nr_test * 100,
				nr_right, nr_test);
		nr_iter ++;
	};
	trainer.train(rbm_spool,
			ssprintf("%s/rbm.txt",
				Config::get("system")["output_dir"].
					as_string().c_str()).c_str());
}

void reconstruct_test(const char *config_fpath) {
	Config::set_fpath(config_fpath);
	SamplePool spool;
	spool.load_from_conf();
	std::vector<RBMTrainer::sample_t> test_spool;
	auto test_buf = gen_rbm_sample(spool.test, test_spool);
	RBMTrainer trainer(RBM_SAMPLE_SIZE, NR_HIDDEN);
	trainer.load("output/rbm-45.txt");
	FILE *fout = fopen("/tmp/test_result.txt", "w");
	int idx = 0;
	for (auto &i_: test_spool) {
		float recon_prob_orig[RBM_SAMPLE_SIZE];
		auto recon_prob = recon_prob_orig + PATCH_WIDTH * PATCH_HEIGHT;
		auto i = const_cast<RBMTrainer::mutable_sample_t>(i_);
		set_sample_label(i, 0);
		trainer.reconstruct(i, recon_prob_orig);
		int truth = spool.test[idx].get_groundtruth(), predict = 0;
		auto max_prob = recon_prob[0];
		for (int j = 1; j < 10; j ++)
			if (recon_prob[j] > max_prob) {
				max_prob = recon_prob[j];
				predict = j;
			}
		std::string fout;
		if (predict != truth) 
			fout = ssprintf("/tmp/out/bad-%d-%d-%d.png", truth, predict, idx);
		else
			fout = ssprintf("/tmp/out/good-%d-%d-%d.png", truth, predict, idx);
		Image img(PATCH_WIDTH, PATCH_HEIGHT);
		for (int r = 0; r < PATCH_HEIGHT; r ++)
			for (int c = 0; c < PATCH_WIDTH; c ++)
				img.at(r, c) = i[r * PATCH_WIDTH + c] ? 255 : 0;
		img.save(fout.c_str());
		spool.test[idx].to_image().save(ssprintf("/tmp/out/orig-%d.png",
					idx).c_str());
		idx ++;
	}
	fclose(fout);
}

int main(int argc, char **argv) {
	if (argc == 1) {
		fprintf(stderr, "usage: %s <command>\n", argv[0]);
		return -1;
	}

	if (!strcmp(argv[1], "train")) {
		dassert(argc == 3);
		train(argv[2]);
	} else if (!strcmp(argv[1], "reconstruct_test")) {
		dassert(argc == 3);
		reconstruct_test(argv[2]);
	}
}

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

