#!/usr/bin/env python2
# -*- coding: utf-8 -*-
# $File: visualize_weight.py
# $Date: Mon Nov 18 21:14:21 2013 +0800
# $Author: jiakai <jia.kai66@gmail.com>

import cv2
import numpy as np
import os.path

OUTDIR = '/tmp/out'
INPUT_FILE = 'output/rbm-45.txt'
PATCH_WIDTH = 13
PATCH_HEIGHT = 17
NR_PXL = PATCH_WIDTH * PATCH_HEIGHT

with open(INPUT_FILE) as fin:
    nr_vis, nr_hid = map(int, fin.readline().split())
    assert nr_vis == PATCH_WIDTH * PATCH_HEIGHT + 10, nr_vis
    fin.readline()
    fin.readline()
    weight = map(float, fin.readline().split())
    assert len(weight) == nr_hid * nr_vis
    weight = np.array(weight).reshape((nr_vis, nr_hid)).T
    for idx, row in enumerate(weight):
        row = row[:NR_PXL].reshape((PATCH_HEIGHT, PATCH_WIDTH))
        rmin = np.min(row)
        rmax = np.max(row)
        row -= rmin
        row *= 255 / (rmax - rmin)
        row = np.array(row, dtype = 'uint8')
        #cv2.imshow('x', row)
        #cv2.waitKey(-1)
        cv2.imwrite(os.path.join(OUTDIR, '{:03}.png'.format(idx)), row)

