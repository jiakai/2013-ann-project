% $File: report.tex
% $Date: Mon Nov 18 23:11:35 2013 +0800
% $Author: jiakai <jia.kai66@gmail.com>

\documentclass[a4paper]{article}
\usepackage{amsmath,amssymb,amsthm,fontspec,verbatim,graphicx}
\usepackage[hyperfootnotes=false,colorlinks,linkcolor=black,anchorcolor=black,citecolor=black]{hyperref}

\usepackage{biblatex}
\bibliography{refs.bib}
\defbibheading{bibliography}{\section{References}}

\usepackage[top=2in, bottom=1.5in, left=1in, right=1in]{geometry}


\newcommand{\addplot}[1]{\begin{center}
	\includegraphics[width=0.6\paperwidth]{#1}
\end{center}}

\newcommand{\addfigurewidth}[4]{\begin{figure}[ht!]\begin{center}
	\includegraphics[width=#4\paperwidth]{#1}
	\caption{#2\label{fig:#3}}
\end{center}\end{figure}}
\newcommand{\addfigure}[3]{\addfigurewidth{#1}{#2}{#3}{0.6}}

% \textref{marker}{text}
\newcommand{\textref}[2]{\hyperref[#1]{#2}}
\newcommand{\figref}[1]{\hyperref[fig:#1]{Figure~\ref*{fig:#1}}}
\newcommand{\eqnref}[1]{\hyperref[eqn:#1]{(\ref*{eqn:#1})}}
\newcommand{\lemref}[1]{\hyperref[lemma:#1]{Lemma~\ref*{lemma:#1}}}
\newcommand{\secref}[1]{\hyperref[sec:#1]{Section~\ref*{sec:#1}}}

\newcommand{\deriv}[2]{\dfrac{\partial{#1}}{\partial{#2}}}
\renewcommand{\vec}[1]{\boldsymbol{#1}}
\newcommand{\hidden}{\mathcal{H}}
\newcommand{\visible}{\mathcal{V}}


\title{Report for the Nerual Network Project}
\author{Kai Jia\\Department of Computer Science and Technology\\
Tsinghua University}
\date{\today}

\begin{document}
\maketitle
\tableofcontents

\section{Goal and Method}
In this project I implemented back-propagation training for a classical 2-layer
feed-forward network and also CD-1 learning for RBM; both networks are trained and
evaluated on the handwritten digit recognition task. This project aims to help
me gain more insight into neural networks, and also compare different algorithms
and parameters.

\section{Algorithm}
\subsection{Preprocessing of Digit Patches\label{sec:preprocessing}}
% f{{{
The provided data is $96\times 96$ patches with the digit at upper-left corner,
demonstrated in \figref{data-example}.
\addfigure{../data-example.png}{Examples of Provided Data}{data-example}
Preprocessing is required to crop out the digit and scale it to a unified size.
Note number 3 in \figref{data-example}, showing the data may contain noise (a
white dot) in black areas. The proposed preprocessing algorithm can be formulated
as following, assuming $W$ and $H$ are the desired width and height of patches:
\begin{enumerate}
	\item Find all connected components composed of white points where
		two horizontally/vertically/diagonally adjacent points at considered to
		be connected.
	\item Omit those components whose size is less than \verb|STOKE_AREA_THRESH|
		(currently defined to be 30).
	\item Find the minimal bounding box with aspect ratio equal to $W/H$ and
		place the digit at the center of the box.
	\item Scale the patch in the box to $W \times H$. This is achieved by
		averaging the corresponding pixels on original patch for each target
		pixel.
\end{enumerate}
See \figref{data-example-processed} for a subset of cropped and scaled
patches, where I set $W = 13, H = 17$.
\addfigurewidth{../patch-example-13x17.png}
{Examples of Cropped Patches}{data-example-processed}{0.4}

Note that for RBM training, which only allows binary input, the patches also
have to be thresholded to binary value; I use $1.2t$ as threshold, where $t$
is the average pixel intensity. A subset of such patches is shown in
\figref{data-example-processed-thresh}. After preprocessing, the patches are fed
as network input directly; no other features are extracted.
\addfigurewidth{../patch-example-13x17-thresh.png}
{Examples of Thresholded Cropped Patches}{data-example-processed-thresh}{0.4}

% f}}}

\subsection{BP Training}
% f{{{
\subsubsection{Network Structure\label{sec:bpstructure}}
I used a 2-layer perceptron (a middle layer and an output layer) with some
heuristic information encoded in its structure. Sigmoid is used as the active
function.

Each neuron in the middle layer is only connected to a rectangular region of the
input patch. Such regions are densely sampled on the input patch, with fixed
size and sliding step; to capture information long straight strokes, I also
included ``bar-like'' regions, which have the same width or height as the
original patch. Such structure could encode neighbour information and also
reduce the number of weights needed to be trained.

For the output layer, I tried two methods. The first is to train a network for
each digit, using this digit as positive sample and all others as negative,
resulting a network suitable for parallel training. When testing, the predicted
digit is the one with highest weight. However, in this way I could only achieve
an accuracy of $93.2\%$. Later I fallback to classical methods, using
10 output units and 1-of-K encoding scheme, whose results are discussed in
detail in later sections.

\subsubsection{Training Algorithm}
The basic idea for training a multilayer perceptron network is to define an
optimization object function and adjust the weights so that the function
evaluated on training samples could be minimized. 

The initial weights are taken from a normal distribution with mean 0 and
variance 1. It should be noted that the weight update could be either performed
after processing all samples, or update immediately after feeding one sample;
the former is called batch update. See \secref{bp} for the details on how to
update weights.

Another problem is when to stop training. The data is separated into training
set, validation set and test set. The training error is evaluated after each
iteration, and if the error on validation set becomes greater than the average
of errors of recent few iterations, it indicates overfitting and training is
aborted.

\subsubsection{Back-propagation\label{sec:bp}}
The optimization object is to minimize average mean-squared-error for all
training samples. Derivations in this section are based on that in
\cite{bishop2006pattern}.

For neuron $j$, define
\begin{equation}
	a_j = \sum_{i} w_{ji} z_i
\end{equation}
where $z_i$ is the output of neuron $i$ and $w_{ji}$ is the weight from $i$ to
$j$. Note that $z_i = h(a_i)$, where $h$ is the active function and
$h(x) = \dfrac{1}{1+e^{-x}}$ in my network.

For training sample $n$, let $E_n$ denote its contribution to total error:
\begin{equation}
	E_n = \dfrac{1}{2}\sum_k (y_{nk} - t_{nk})^2
\end{equation}
where $y_{nk}$ is the value of output unit $k$ and $t_{nk}$ is the ground truth
of this element in output for the sample.

To minimize $E_n$, we adopt gradient descent method and take the derivative of
$E_n$ to $w_{ji}$:
\begin{equation}
	\deriv{E_n}{w_{ji}} = \deriv{E_n}{a_j}\deriv{a_j}{w_{ji}}
\end{equation}

We define $\delta_j = \deriv{E_n}{a_j}$ and it could be easily shown
$\deriv{a_j}{w_{ji}}=z_j$; thus we have
\begin{equation}
	\deriv{E_n}{w_{ji}} = \delta_j z_i \label{eqn:Enderiv}
\end{equation}

For output unit $k$, we have 
\begin{eqnarray}
	\delta_k & = & \deriv{E_n}{a_k} \nonumber \\
		& = & (y_{nk} - t_{nk})h'(a_k) \label{eqn:bp-output}
\end{eqnarray}

For unit $j$ in middle layers:
\begin{eqnarray}
	\delta_j & = & \deriv{E_n}{a_j} \nonumber \\
		& = & \sum_k \deriv{E_n}{a_k}\deriv{a_k}{a_j} \nonumber \\
		& = & \sum_k \delta_k h'(a_j) w_{kj} \nonumber \\
		& = & h'(a_j) \sum_k \delta_k w_{kj} \label{eqn:bp-middle}
\end{eqnarray}
where $k$ is a unit to which $j$ connects.

We can compute $\deriv{E_n}{w_{ji}}$ efficiently with the help of
\eqnref{Enderiv}, \eqnref{bp-output} and \eqnref{bp-middle} and update $w_{ji}$
using $-\eta \deriv{E_n}{w_{ji}}$, where $\eta$ is the learning rate.

% f}}}

\subsection{RBM Training}
% f{{{
Unlike feed-forward neural networks which is based on discriminative
perceptrons, restricted Boltzmann machine(RBM) is a generative model, which
could output $p(\text{image})$ instead of $p(\text{label}|\text{image})$.
It is basically a restricted form of deep belief network composed of a visible
layer and a hidden layer. Connections only exist between these two layers,
making it suitable for parallelized training. Refer to
\cite{hinton2010practical} for a more comprehensive introduction.

The RBM model can be applied to classification problems in at least two ways:
\begin{enumerate}
	\item Use the hidden layer as middle layer for multilayer perceptron and the
		weight as initial weight; add an output layer and then adopt BP
		training. The training of RBM is also called pre-training.
	\item Add 10 extra dimensions to each training sample and encode its class
		in the extra dimensions, using 1-of-K scheme. When making prediction for
		a new sample, set the extra dimensions to 0 and after reconstruction
		find the one of highest probability to be 1, which represents the final
		prediction.
\end{enumerate}

I think the second method utilizes its generative nature and enables us to study
RBM independently, so I chose the latter one.

I used CD-1 learning. The derivation of training algorithm is quite complex and
omitted here; but the result is very simple, summarized as following:

Let $\hidden$ denote the set of hidden units and $\visible$ the visible ones,
$w_{ij}$ the weight between unit $i, j$ for $i\in\visible$, $j\in\hidden$.
$v_i, h_j \in \{0, 1\}$ for $i\in\visible, j\in\hidden$ is a random variable
denoting the activation status for a unit. We have
\begin{eqnarray}
	P(h_j = 1 | \vec{v}) & = & \sigma\left(b_j + \sum_{i\in\visible} v_iw_{ij}\right) \\
	P(v_i = 1 | \vec{h}) & = & \sigma\left(a_i + \sum_{j\in\hidden} h_jw_{ij}\right)
\end{eqnarray}
where $a_i$ and $b_j$ are the biases.

For a given sample $\vec{v}$, the corresponding $\vec{h}$ could be computed and
sampled to 0 or 1 with the resulting probability; let $\vec{h'}$ denote the
sampled result, and $\vec{v'}$ could be computed using the same algorithm from
$h'$. Computing $\vec{v'}$ from initial $\vec{v}$ is called reconstruction.

Then weight updates are given by:
\begin{eqnarray}
	\Delta w_{ij} & = & \epsilon(v_ih_j - v_i'h_j') \\
	\Delta a_i & = & \epsilon(v_i - v_i') \\
	\Delta b_j & = & \epsilon(h_j - h_j')
\end{eqnarray}
where $\epsilon$ is the learning rate. In actual implementation, using
probability value instead of sampled binary state for $v_i'$ and $h_j'$ reduces
sampling noise and yields better results.
% f}}}

\section{Experiment}
\subsection{Experiment Setup}
The feed-forward network is trained on my own laptop with 2.5GHz Intel Core i5;
the RBM is trained on a workstation with 2.5GHz Intel Xeon E5630,
8 physical cores. A total number of 13999 samples are provided; I randomly
divided them into training set, validation set and test set, each having $40\%,
30\%, 30\%$ of the samples respectively. All following experiments are conducted
on the same data set.

The code is in \verb|BP| and \verb|RBM| directories, and the model and log are
stored in \verb|{BP,RBM}/output|.

\subsection{BP Training}
% f{{{
As mentioned in \secref{bpstructure}, I use a localized network structure
focusing on features of neighbouring pixels. The network can be controlled by
following parameters:
\begin{center}\begin{tabular}{ll}
	$P_w$ & width of input patch \\
	$P_h$ & height of input patch \\
	$N_s$ & size of input rectangular block for middle-layer neurons \\
	$N_t$ & sliding step of input rectangular block for middle-layer neurons \\
	$V_s$ & width of vertical bar blocks \\
	$V_t$ & sliding step of vertical bar blocks \\
	$H_s$ & height of horizontal bar blocks \\
	$H_t$ & sliding step of horizontal bar blocks
\end{tabular}\end{center}
\addfigurewidth{../bp-param.png}{Explanation of Parameters Controlling Network
Structure}{bp-param}{0.4}
See \figref{bp-param} for a demonstration of the meaning of those parameters;
note that $V_s$ and $V_t$ are similar to $H_s$ and $H_t$ except that the bar is
vertical, and thus is omitted in the figure for the sake of compactness. The
results are listed in \figref{bp-results}.

In order to compare the proposed network structure with the traditional
structure in which each neuron in the middle layer is connected to all input
units, I also trained m5, containing the same number of middle layer neurons as
m0. 

\begin{figure}[ht!]\begin{minipage}{\textwidth}
\begin{center}\begin{tabular}{l|l|l|l|l|l|l|l|l|l|l|l|l}
	\hline 
	id & $P_w$ & $P_h$ & $N_s$ & $N_t$ & $V_s$ & $V_t$ & $H_s$ & $H_t$ &
		batch\footnote{whether batch update is used; -1 for not using,
			otherwise it's the number of iteration after which batch updating is
			used} &
		time\footnote{real training time in seconds} &
		iter\footnote{number of iterations before training stops} &
		accuracy\footnote{evaluated on a completely-separated test set}\\ \hline
	m0 & 13 & 17 & 5 & 1 & 2 & 1 & 3 & 1 & -1 & 109 & 147 & $97.36\%$  \\
	m1 & 16 & 20 & 9 & 1 & 4 & 1 & 6 & 1 & -1 & 1535 & 1004 & $96.61\%$ \\
	m2 & 13 & 17 & 8 & 1 & 2 & 1 & 3 & 1 & -1 & 119 & 176 & $96.79\%$ \\
	m3 & 13 & 17 & 5 & 1 & 2 & 1 & 3 & 1 & 50\footnote{if using batch learning
		from the start, the convergence is very slow; training error drops
		about 1e-5 after one iteration, and accuracy is still about $10\%$ after
		2000 iterations} & 145 & 232 & $96.96\%$  \\
	m5\footnote{fully-connected middle layer; see text for details}
		& 13 & 17 & N/A & N/A & N/A & N/A & N/A & N/A & -1 & 3099 & 1040 &
		$86.52\%$ \\
		\hline
\end{tabular}
\caption{\label{fig:bp-results}BP Training Results}
\end{center}
\end{minipage}\end{figure}

The misclassified numbers of network m0 are show in \figref{bp-bad}, where the
$i$th row contain the numbers classified to be $i$ by the network. Upper-left
corner is $(0, 0)$. Some examples are hard even for human beings, and I suspect
labels of some samples are wrong; for example, the ground truth of digit at (4,
3) is 9, but I think it looks more like 4.
\addfigurewidth{../bp-bad.png}{Misclassified Digits by Network m0}{bp-bad}{0.3}

It is also interesting to inspect the training error of each iteration, as
plotted in \figref{iter-error}.
\addfigurewidth{../bp-log.png}{Error Metrics During Training}{iter-error}{0.45}


% f}}}

\subsection{RBM Training}
% f{{{
One thing I learned from RBM training is the importance of learning rate;
accuracy increased from $89\%$ to $92\%$ when I changed it from 0.3 to 0.05.

To compare it with BP, I also trained a BP network m4, whose only difference
from aforementioned m0 is that m4 uses the same training data as RBM, which is
thresholded to binary value; refer to \secref{preprocessing} for more details.
m4 achieved an accuracy of $96.17\%$.


\addfigurewidth{../rbm-weight.png}{Visualization of RBM Weights}{rbm-weight}{0.8}
\addfigurewidth{../rbm-recon.png}{Reconstruction of RBM; left column is input,
right column is reconstructed result; top row is right result, bottom row is
wrong result ($i$th row contains patches classified as $i$)}{rbm-recon}{0.4}

250 hidden units are used. The weights are visualized in \figref{rbm-weight},
from which we could find some clues of the original digit shapes.  The
reconstruction results are shown in \figref{rbm-recon}.

I did not perform much parameter fine-tuning. During training, the best accuracy
on test set is $92.47\%$ at iteration 45; then it begins to decrease, indicating
overfitting. I deveted some effort into parallization and code optimization, and
the training procedure on the workstation took less than 3 minutes.

% f}}}

\section{Conclusion and Discussion}
The best classification result achived by me is from the feed-forward network
m0, whose accuracy on a separate test set reaches $97.36\%$. It contains 144
neurions and 5512 weights, and could be trained in less than 2 minutes. It is
already know that the fitting capability of multilayer perceptrons is so strong
that any real-valued function under some constraints could be approximated to
arbitrary precision, provided with sufficient number of middle-layer units.
However, the optimization function is not convex and optimzing by
gradient descent may end up at a local minimum. The sudden drop of training
error in \figref{iter-error} also supports this argument. Theoretically
speaking, only after 5512 iterations could it be possible to span the parameter
space of 5512 dimensions for the proposed network structure. Thus it is really
important to limit parameter space dimensionality, and experiment result
supports this idea. In practice, it may help to introduce human heuristic
information when designing network structure to achive better balance of
generalization ability and model complexity.

For RBM, it is really interesting to visualize the weights and reconstructed
digits. However, due to limited time and effort, I dit not try hard to tune the
parameters. Better results may be achived by changing learning rate, number of
hidden nurons, changing size of input patch and stacking multiple RBMs together.
Though showing worse performance on this specific experiment task, RBM as a
generative model could do much more, such as correction of corrupted data and
novelty detection; it could also produce a more sensible confidence value than
feed-forward networks in classification tasks.

Anyway, there is much more worth learning, exploring and discovering in the
fields of neural networks. Hope one day it could solve many challenges in AI.

\printbibliography
\end{document}

% vim: filetype=tex foldmethod=marker foldmarker=f{{{,f}}}

