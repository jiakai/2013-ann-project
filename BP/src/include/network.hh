/*
 * $File: network.hh
 * $Date: Mon Nov 04 17:02:19 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#pragma once

#include "math.hh"
#include "sample.hh"
#include <vector>

struct LearningParam {
	float rate;
};

class NeuralNetwork {
	protected:
		using num_t = double;

		struct Node;
		struct Arc {
			union From {
				Node *ptr;
				int idx;
			};
			From from;
			num_t weight, gradient = 0;

			Arc(int from_idx, num_t w):
				weight(w)
			{ from.idx = from_idx; }

			Arc(Node *from_ptr, num_t w):
				weight(w)
			{ from.ptr = from_ptr; }
		};

		struct Node {
			std::vector<Arc> in;
			num_t delta_sum,
				  output,
				  output_deriv;

			static inline double h(double x) {
				return 1 / (1 + exp(-x));
			}

			static inline double h_deriv(double x) {
				auto t = h(x);
				return t * (1 - t);
			}

			void init_err(num_t expected_output) {
				delta_sum = output - expected_output;
			}

			void calc_output() {
				num_t s = 0;
				for (auto &i: in)
					s += i.weight * i.from.ptr->output;
				output = h(s);
				output_deriv = h_deriv(s);
				dassert(is_good_r(output));
				dassert(is_good_r(output_deriv));
			}
		};

		//! must sort in topology order
		std::vector<Node> m_node, m_bias_node;
		Node *m_output_start;

		//! indices of nodes in input & output layers
		int m_nr_input, m_nr_output;

		LearningParam m_param;

		void backpropagation_calc_gradient(const num_t *expected_output,
				double weight = 1);
		/*!
		 * \return avg weight delta
		 */
		double backpropagation_update(double factor = 1);
		void eval();

		NeuralNetwork() = default;

		void finish_init();

		void save(const std::string &fpath);
		void load(const std::string &fpath);
	public:
		NeuralNetwork(const NeuralNetwork&) = delete;
		NeuralNetwork(const NeuralNetwork&&) = delete;
		NeuralNetwork& operator = (const NeuralNetwork&) = delete;
};


/*!
 * \brief neural network for recognizing a single digit
 */
class SingleDigitNN: public NeuralNetwork {
	/*!
	 * \return weight for this data
	 */
	double train_data_point(const Sample &sample);
	int m_target_digit;

	std::string get_fpath() const;

	void save() {
		NeuralNetwork::save(get_fpath());
	}

	public:
		SingleDigitNN(int target_digit);

		/*!
		 * \brief train the network to recognize target digit
		 */
		void train(const SamplePool &data);

		void load() {
			NeuralNetwork::load(get_fpath());
		}

		/*!
		 * \brief calc mean square error
		 */
		double calc_mse(const std::vector<Sample> &sample);
		double calc_misclassify(const std::vector<Sample> &sample);

		double eval(const Sample &sample);
};

/*!
 * \brief neural network for recognizing multiple digits
 */
class MultipleDigitNN: public NeuralNetwork {
	void train_data_point(const Sample &sample);

	std::string get_fpath() const;

	void save() {
		NeuralNetwork::save(get_fpath());
	}

	public:
		MultipleDigitNN();

		/*!
		 * \brief train the network to recognize target digit
		 */
		void train(const SamplePool &data);

		void load() {
			NeuralNetwork::load(get_fpath());
		}

		/*!
		 * \brief calc mean square error
		 */
		double calc_mse(const std::vector<Sample> &sample);
		double calc_misclassify(const std::vector<Sample> &sample);

		int eval(const Sample &sample);
};

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

