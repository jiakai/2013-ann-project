/*
 * $File: sample.hh
 * $Date: Mon Nov 04 22:43:26 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#pragma once

#include "image.hh"
#include "globalparam.hh"

#include <cstdio>

/*!
 * \brief represents a raw digit sample
 */
class Sample {
	pixel_t m_img_data[PATCH_HEIGHT][PATCH_WIDTH];
	int m_groundtruth;

	static Image crop(Image &src);

	public:

		/*!
		 * \brief load this sample from pnt file
		 */
		bool load_from_pnt(FILE *fin, bool perform_thresholding);


		/*!
		 * \brief convert this sample to image type
		 */
		Image to_image() {
			return Image::from_existing_buf(PATCH_WIDTH, PATCH_HEIGHT,
					reinterpret_cast<pixel_t*>(m_img_data));
		}

		int get_groundtruth() const {
			return m_groundtruth;
		}

		const pixel_t *data() const {
			return reinterpret_cast<const pixel_t*>(m_img_data);
		}
};

struct SamplePool {
	std::vector<Sample> train, validation, test;

	void load_from_conf();
};

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

