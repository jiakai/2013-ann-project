/*
 * $File: classifier.hh
 * $Date: Sun Oct 13 20:36:29 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#pragma once

#include "network.hh"

#include <list>

class Classifier {
	std::list<SingleDigitNN> m_network;

	public:
		Classifier() {
			for (int i = 0; i < 10; i ++)
				m_network.emplace_back(i);
			for (auto &i: m_network)
				i.load();
		}

		int classify(const Sample &data, double &confidence) {
			int mc_v = -1;
			double mc = -1, mc2 = -1;
			int v = 0;
			for (auto &i: m_network) {
				auto conf = i.eval(data);
				if (conf > mc) {
					mc_v = v;
					mc2 = mc;
					mc = conf;
				} else if (conf > mc2)
					mc2 = conf;
				v ++;
			}
			confidence = mc / mc2;
			return mc_v;
		}
};

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

