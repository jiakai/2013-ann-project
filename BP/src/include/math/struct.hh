/*
 * $File: struct.hh
 * $Date: Sun Oct 13 00:32:38 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#pragma once

#include "utils.hh"

#include <cstring>

template<typename scalar_t>
class Rect {
	scalar_t m_x0, m_y0, m_width, m_height;

	public:	
		Rect(scalar_t x0 = 0, scalar_t y0 = 0,
				scalar_t w = 0, scalar_t h = 0):
			m_x0(x0), m_y0(y0), m_width(w), m_height(h)
		{ dassert(x0 >= 0 && y0 >= 0 && w >= 0 && h >= 0); }

		scalar_t x0() const { return m_x0; }
		scalar_t y0() const { return m_y0; }
		scalar_t x1() const { return m_x0 + m_width; }
		scalar_t y1() const { return m_y0 + m_height; }
		scalar_t width() const { return m_width; }
		scalar_t height() const { return m_height; }
		scalar_t area() const { return m_width * m_height; }

		void x0(scalar_t x) { m_x0 = x; }
		void x1(scalar_t x) { m_width = x - m_x0; }
		void y0(scalar_t y) { m_y0 = y; }
		void y1(scalar_t y) { m_height = y - m_y0; }
		void width(scalar_t w) { m_width = w; }
		void height(scalar_t h) { m_height = h; }

		scalar_t& x0() { return m_x0; }
		scalar_t& y0() { return m_y0; }
		scalar_t& width() { return m_width; }
		scalar_t& height() { return m_height; }

		void apply_to_coord(std::function<scalar_t(const scalar_t&)> cb) {
			int x1 = cb(this->x1()),
				y1 = cb(this->y1());
			m_x0 = cb(m_x0); m_y0 = cb(m_y0);
			m_width = x1 - m_x0;
			m_height = y1 - m_y0;
		}

		void apply_to_xywh(std::function<scalar_t(const scalar_t&)> cb) {
			m_x0 = cb(m_x0); m_y0 = cb(m_y0);
			m_width = cb(m_width); m_height = cb(m_height);
		}

		/*!
		 * \brief area of intersection
		 */
		scalar_t intersect(const Rect &r) const {
			int dx = min(x1(), r.x1()) - max(x0(), r.x0()),
				dy = min(y1(), r.y1()) - max(y0(), r.y0());
			if (dx < 0 || dy < 0)
				return 0;
			return dx * dy;
		}
};

using IRect = Rect<int>;

template<typename scalar_t>
class Matrix {
#define CHECK_RANGE(r, c) dassert(r >= 0 && r < m_height && \
		c >= 0 && c < m_width)

	int m_allocated_size;
	protected:
		int m_width, m_height;
		std::shared_ptr<scalar_t> m_data;

	public:
		Matrix():
			m_allocated_size(0), m_width(0), m_height(0)
		{}

		Matrix(int w, int h, std::shared_ptr<scalar_t> data):
			m_allocated_size(w * h), m_width(w), m_height(h), m_data(data)
		{}

		Matrix(int w, int h):
			m_allocated_size(w * h), m_width(w), m_height(h),
			m_data(create_auto_buf<scalar_t>(w * h, true))
		{}

		static Matrix from_existing_buf(int w, int h, scalar_t *buf) {
			return Matrix(w, h, std::shared_ptr<scalar_t>(buf, [](scalar_t*){}));
		}

		int width() const { 
			return m_width;
		}

		int height() const {
			return m_height;
		}

		int area() const {
			return m_width * m_height;
		}

		/*!
		 * \brief return a pointer to the element at r'th row, c'th col
		 */
		const scalar_t* ptr(int r = 0, int c = 0) const {
			CHECK_RANGE(r, c);
			return m_data.get() + r * m_width + c;
		}

		/*!
		 * \brief return a pointer to the element at r'th row, c'th col
		 */
		scalar_t* ptr(int r = 0, int c = 0) {
			CHECK_RANGE(r, c);
			return m_data.get() + r * m_width + c;
		}

		/*!
		 * \brief set the dimension of this matrix (allocate new buf if
		 *		necessary)
		 */
		Matrix& set_dimension(int width, int height) {
			if (width * height > m_allocated_size)
				reset(width, height);
			else {
				m_width = width;
				m_height = height;
			}
			return *this;
		}

		/*!
		 * \brief set the dimension of this matrix (allocate new buf if
		 *		necessary)
		 */
		template <typename T>
		Matrix& set_dimension(const Matrix<T> &m) {
			return set_dimension(m.m_width, m.m_height);
		}

		/*!
		 * \brief allocate a new buffer filled with 0 for this matrix
		 */
		Matrix& reset(int width, int height) {
			m_width = width;
			m_height = height;
			m_allocated_size = width * height;
			m_data = create_auto_buf<scalar_t>(width * height, true);
			return *this;
		}

		/*!
		 * \brief return the element at a specific location
		 */
		const scalar_t& at(int r, int c) const {
			return *ptr(r, c);
		}

		/*!
		 * \brief return the element at a specific location
		 */
		scalar_t& at(int r, int c) {
			return *ptr(r, c);
		}

		/*!
		 * \brief return the element at a specific location; if out of boundary,
		 *		return the default value
		 */
		scalar_t at(int r, int c, scalar_t default_) const {
			if (r < 0 || r >= m_height || c < 0 || c >= m_width)
				return default_;
			return m_data.get()[r * m_width + c];
		}

		/*!
		 * \brief fill the matrix using memset
		 */
		void fill_memset(int val) {
			memset(m_data.get(), val, m_width * m_height * sizeof(scalar_t));
		}

		scalar_t* begin() {
			return m_data.get();
		}

		scalar_t* end() {
			return m_data.get() + m_width * m_height;
		}

		const scalar_t* begin() const {
			return m_data.get();
		}

		const scalar_t* end() const {
			return m_data.get() + m_width * m_height;
		}

		bool empty() const {
			return !m_width || !m_height;
		}

		Matrix<scalar_t> copy() const {
			Matrix<scalar_t> ret(m_width, m_height,
					create_auto_buf<scalar_t>(m_width * m_height));
			memcpy(ret.m_data.get(), m_data.get(),
					sizeof(scalar_t) * m_width * m_height);
			return ret;
		}

		void put_sub(const Matrix<scalar_t> &sub, int r0, int c0) {
			dassert(r0 >= 0 && c0 >= 0 &&
					r0 + sub.m_height <= m_height &&
					c0 + sub.m_width <= m_width);
			for (int i = r0; i < r0 + sub.m_height; i ++)
				memcpy(ptr(i, c0), sub.ptr(i - r0),
						sizeof(scalar_t) * sub.m_width);
		}

		Matrix get_sub(const IRect &region) const {
			dassert(region.x1() <= m_width && region.y1() <= m_height
					&& region.area());
			Matrix rst(region.width(), region.height());
			for (int r = 0; r < region.height(); r ++)
				memcpy(rst.ptr(r), ptr(r + region.y0(), region.x0()),
						region.width() * sizeof(scalar_t));
			return rst;
		}
};


// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}
