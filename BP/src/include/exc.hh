/*
 * $File: exc.hh
 * $Date: Sat Oct 12 22:16:29 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#pragma once
#include "utils.hh"

#include <exception>
#include <cstdarg>

#define IMPL_ERR_CONSTRUCTOR(name) \
	__attribute__((format(printf, 2, 3))) \
	name(const char *fmt, ...) { \
		va_list ap; \
		va_start(ap, fmt); \
		m_msg = svsprintf(fmt, ap); \
		va_end(ap); \
	} 

class HwdrErr: public std::exception {
	protected:
		std::string m_msg;
	public:
		IMPL_ERR_CONSTRUCTOR(HwdrErr);

		HwdrErr() = default;

		const char *what() const throw (){
			return m_msg.c_str();
		}
};

class AssertionErr: public HwdrErr {
	public:
		IMPL_ERR_CONSTRUCTOR(AssertionErr)
};

#undef IMPL_ERR_CONSTRUCTOR

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

