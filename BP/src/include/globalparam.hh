/*
 * $File: globalparam.hh
 * $Date: Thu Oct 24 09:28:11 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#pragma once

constexpr double WEIGHT_INIT_VARIANCE = 1;
constexpr int STOKE_AREA_THRESH = 30;
constexpr int PATCH_WIDTH = 13, PATCH_HEIGHT = 17,
		  FTR_PATCH_SIZE = 5, FTR_SLIDE_STEP = 1,
		  FTR_VBAR_WIDTH = 2, FTR_VBAR_SLIDE = 1,
		  FTR_HBAR_HEIGHT = 3, FTR_HBAR_SLIDE = 1;

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

