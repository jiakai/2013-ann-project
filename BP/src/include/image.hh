/*
 * $File: image.hh
 * $Date: Mon Nov 18 08:55:08 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#pragma once

#include <memory>
#include <cstdint>

#include "math.hh"
#include "math/struct.hh"

/*!
 * \file
 * \brief definitions of basic types/structures for image manipulation
 */

typedef uint8_t pixel_t;

class Image: public Matrix<pixel_t> {
	public:
		using mouse_callback_t = std::function<void(int ev, int x, int y)>;
		using Matrix<pixel_t>::Matrix;

		Image() = default;

		Image(const Matrix<pixel_t> &img):
			Matrix<pixel_t>(img)
		{}

		/*!
		 * \brief display the image on screen
		 * \param waitkey if non zero, positive value indicates wait for key
		 *		press in this time [ms], negative value indicates wait forever
		 */
		void display(int waitkey = 0, const char *win_title = "image",
				 mouse_callback_t mouse_callback = mouse_callback_t()) const;

		void draw_rect(const IRect &rect, int color = 255);

		Image downscale(float scale) const;

		Image fast_scale_up(float scale) const;

		/*!
		 * \brief flip around y axes
		 */
		void flip();

		void save(const char *fout) const;

		/*!
		 * \brief rotate for n * 90 degrees counter-clockwise
		 */
		Image rotate_quadrant(int n) const;

		static Image load(const char *fpath);

		static int mouse_ev_left_click();
};

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

