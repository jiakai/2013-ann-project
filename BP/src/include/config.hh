/*
 * $File: config.hh
 * $Date: Sat Oct 12 22:47:37 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#pragma once

#include "utils.hh"
#include "exc.hh"

#include <ctime>
#include <vector>

class Config {
	class Impl;
	class ImplBase {
		public:
			virtual ~ImplBase() {}
	};
	std::shared_ptr<ImplBase> m_impl;
	std::string m_path;
	static std::string sm_fpath;
	static time_t sm_file_last_modify;

	template<typename T>
	T get_val() const;

	template<typename T>
	std::vector<T> get_val_vec(bool allow_absent) const;

	Config(std::shared_ptr<ImplBase> impl, const std::string &path):
		m_impl(impl), m_path(path)
	{}

	Config() = default;
	static Config sm_instance;

	// impl should only be accessed through these two functions
	Impl& impl(); 
	const Impl& impl() const; 

	public:
		class NoValErr: public HwdrErr {
			public:
				NoValErr(const char *name):
					HwdrErr("config `%s' not found", name)
			{ }
		};

		std::string as_string() const;
		int as_int() const;
		double as_double() const;

		std::vector<std::string> as_string_vec(bool allow_absent = false) const;
		std::vector<int> as_int_vec(bool allow_absent = false) const;
		std::vector<Config> as_vec(bool allow_absent = false) const;

#define IMPL_WITH_DEFAULT_ARG(name, type) \
		type as_##name(const type& def) const { \
			try { \
				return as_##name(); \
			} catch (NoValErr&) { \
				return def; \
			} \
		}

		IMPL_WITH_DEFAULT_ARG(string, std::string);
		IMPL_WITH_DEFAULT_ARG(int, int);
		IMPL_WITH_DEFAULT_ARG(double, double);

#undef IMPL_WITH_DEFAULT_ARG

		static Config get(const std::string &name);

		static void set_fpath(const char *fpath) {
			sm_fpath = fpath;
			sm_file_last_modify = -1;
		}

		Config operator[] (const std::string &name) const;

		~Config() __attribute__((noinline)) {}
};

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

