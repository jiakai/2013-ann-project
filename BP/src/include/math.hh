/*
 * $File: math.hh
 * $Date: Wed Oct 09 22:32:16 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#pragma once

#include "utils.hh"

#include <cstdlib>
#include <cmath>
#include <limits>
#include <algorithm>
#include <random>

/*!
 * \brief whether a real number is not nan or infinite
 */
template<typename T>
static inline bool is_good_r(T x) {
	auto c = std::fpclassify(x);
	return c != FP_NAN && c != FP_INFINITE;
}

template<typename T>
T sqr(const T &v) {
	return v * v;
}

template<typename T>
static inline T dot_product(const T *a, const T *b, size_t len) {
	T s = 0;
	for (size_t i = 0; i < len; i ++)
		s += a[i] * b[i];
	return s;
}

template<typename T>
inline T quadratic_sum(T v0) {
	return v0 * v0;
}

template<typename T, typename... Args>
inline T quadratic_sum(T v0, Args... args) {
	return v0 * v0 + quadratic_sum(args...);
}

template<typename T, typename... Args>
inline T quadratic_sum_sqrt(T v0, Args... args) {
	return sqrt(quadratic_sum(v0, args...));
}

template<typename T>
inline T min(T v0) {
	return v0;
}

template<typename T, typename... Args>
inline T min(T v0, Args... args) {
	T tmp = min(args...);
	if (v0 < tmp)
		return v0;
	return tmp;
}

template<typename T>
inline T max(T v0) {
	return v0;
}

template<typename T, typename... Args>
inline T max(T v0, Args... args) {
	T tmp = max(args...);
	if (v0 > tmp)
		return v0;
	return tmp;
}

template<typename T>
int iceil(T x) {
	return int(ceil(x));
}

template<typename T>
int ifloor(T x) {
	return int(floor(x));
}

template<typename T>
int iround(T x) {
	return int(round(x));
}

template<typename T>
inline bool update_min(T &) {
	return false;
}

template<typename T, typename... Args>
inline bool update_min(T &target, const T &val, Args... args) {
	bool ret = false;
	if (val < target) {
		target = val;
		ret = true;
	}
	ret |= update_min(target, args...);
	return ret;
}

template<typename T>
inline bool update_max(T &) {
	return false;
}

template<typename T, typename... Args>
inline bool update_max(T &target, const T &val, Args... args) {
	bool ret = false;
	if (target < val) {
		target = val;
		ret = true;
	}
	ret |= update_max(target, args...);
	return ret;
}

template<typename T>
inline T numeric_max() {
	return std::numeric_limits<T>::max();
}

template<typename T>
inline T numeric_min() {
	return std::numeric_limits<T>::min();
}

template<typename Iterator>
inline auto find_nth(Iterator begin, Iterator end, int n) -> decltype(*begin) {
	std::nth_element(begin, begin + n, end);
	return *(begin + n);
}

template<typename Container>
inline auto find_nth_ratio(Container &c, double k) -> decltype(*c.begin()) {
	return find_nth(c.begin(), c.end(), ifloor(k * c.size()));
}

template<typename T>
inline bool in_range(const T &x, const T &a, const T &b) {
	return a <= x && x <= b;
}

template<typename IteratorA, typename IteratorB>
auto calc_l2_norm(IteratorA a, IteratorB b, size_t n) -> decltype(*a + *b) {
	decltype(*a + *b) sum = 0;
	for (size_t i = 0; i < n; i ++)
		sum += sqr(a[i] - b[i]);
	return sqrt(sum);
}

/*!
 * \brief thread-safe random number generator
 */
class RandomGenerator {
	using engine_t = std::mt19937_64;
	static engine_t m_engine;
	static std::mutex m_mtx;
	public:
		static double gen() {
			LOCK_GUARD(m_mtx);
			return double(m_engine()) / double(engine_t::max());
		}
};


/*!
 * \brief uniform distribution between [a, b)
 */
static inline double rand(double a, double b) {
	return RandomGenerator::gen() * (b - a) + a;
}

/*!
 * \brief uniform distribution between [a, b)
 */
static inline int irand(int a, int b) {
	return ifloor(RandomGenerator::gen() * (b - a)) + a;
}

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

