/*
 * $File: utils.hh
 * $Date: Sun Oct 13 14:11:58 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#pragma once

#include <cstdarg>
#include <cstring>
#include <ctime>

#include <functional>
#include <memory>
#include <string>
#include <mutex>

#define LOCK_GUARD(mtx) \
	std::lock_guard<decltype(mtx)> __lock_guard_##mtx(mtx)

#define dassert(expr) \
	do { \
		if (!(expr)) \
			__assert_failed__(__FILE__, __func__, \
					__LINE__, # expr); \
	} while (0)


#define log_printf(fmt, ...) \
	__log_printf__(__FILE__, __func__, __LINE__, fmt, ## __VA_ARGS__)

double get_cputime();

class CPUTimer {
	double m_start_time;
	std::function<void(CPUTimer &t)> m_on_destroy;

	public:
		CPUTimer(std::function<void(CPUTimer &t)> on_destroy = [](CPUTimer &){}):
			m_start_time(get_cputime()),
			m_on_destroy(on_destroy)
		{}

		~CPUTimer() {
			m_on_destroy(*this);
		}

		double get_time() const {
			return get_cputime() - m_start_time;
		}

		void reset() {
			m_start_time = get_cputime();
		}
};

template<typename T>
std::shared_ptr<T> create_auto_buf(size_t len, bool init_zero = false) {
	std::shared_ptr<T> ret(new T[len], [](T *ptr){delete []ptr;});
	if (init_zero)
		memset(ret.get(), 0, sizeof(T) * len);
	return ret;
}

double get_realtime();

class ProgressReporter {
	static constexpr double AVG_UPDATE = 0.15;
	int m_cnt_cur_interval, m_cnt_tot, m_target;
	double m_prev_time, m_start_time, m_avg_speed;
	std::string m_name;
	std::mutex m_mtx;

	public:
		/*!
		 * \param target target cnt value, or -1 if unknown
		 */
		ProgressReporter(const std::string &name, int target = -1):
			m_name(name)
		{ reinit(target); }

		void reinit(int target) {
			m_cnt_cur_interval = m_cnt_tot = 0;
			m_target = target;
			m_prev_time = -1;
			m_avg_speed = -1;
			m_start_time = get_realtime();
		}

		void trigger(int delta = 1);

		void done();
};

/*!
 * \brief printf-like std::string constructor
 */
std::string svsprintf(const char *fmt, va_list ap);

std::string ssprintf(const char *fmt, ...)
	__attribute__((format(printf, 1, 2)));

time_t get_mtime(const char *fpath);

/*!
 * \brief peak virtual memory usage
 */
size_t get_peak_vm();

int get_nr_worker_thread();

void disable_ctrl_c();

/*!
 * alignment suitable for arbitrary object
 */
static constexpr int MEM_ALIGN_SIZE = 16;

/*!
 * \brief align to a general memory boundary
 */
static inline size_t mem_align(size_t s) {
	return ((s - 1) / MEM_ALIGN_SIZE + 1) * MEM_ALIGN_SIZE;
}

template<typename T>
static inline T* mem_align(T *ptr) {
	return (T*)mem_align((size_t)ptr);
}

size_t get_file_size(const char *fpath);

// internal implementations, ignore
void __assert_failed__(const char *file, const char *func, int line,
		const char *expr) __attribute__((noreturn));

void __log_printf__(const char *file, const char *func, int line,
		const char *fmt, ...) __attribute__((format(printf, 4, 5)));

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

