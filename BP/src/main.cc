/*
 * $File: main.cc
 * $Date: Mon Nov 18 19:53:24 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "classifier.hh"
#include "network.hh"
#include "config.hh"
#include "exc.hh"
#include <cstring>
#include <random>
#include <thread>

static void convert(const char *fpath_pnt, const char *fpath_raw) {
	FILE *fin = fopen(fpath_pnt, "rb"),
		 *fout = fopen(fpath_raw, "wb");
	if (!fin)
		throw HwdrErr("failed to open %s: %m", fpath_pnt);
	if (!fout)
		throw HwdrErr("failed to open %s: %m", fpath_raw);
	std::vector<Sample> sample;
	int disp = 10;
	while (!feof(fin)) {
		sample.emplace_back();
		if (!sample.back().load_from_pnt(fin, true))
			sample.pop_back();
		if (disp) {
			sample.back().to_image().display(-1);
			disp --;
		}
	}
	std::random_shuffle(sample.begin(), sample.end());
	if (fwrite(sample.data(), sizeof(Sample), sample.size(), fout)
			!= sample.size())
		throw HwdrErr("failed to write output file");
	fclose(fin);
	fclose(fout);
}

static void train_single(const char *config_fpath) {
	Config::set_fpath(config_fpath);
	SamplePool spool;
	spool.load_from_conf();
	/*
	for (auto &i: spool.train)
		i.to_image().display(-1);
		*/

	int cur_digit = 0;
	auto worker = [&]() {
		for (; ;) {
			int d = __sync_fetch_and_add(&cur_digit, 1);
			if (d >= 10)
				return;
			SingleDigitNN snn(d);
			snn.train(spool);
		}
	};
	std::vector<std::thread> worker_thread;
	int nr_worker = get_nr_worker_thread();
	for (int i = 0; i < nr_worker; i ++)
		worker_thread.emplace_back(worker);
	for (auto &i: worker_thread)
		i.join();
}

static void train_multiple(const char *config_fpath) {
	Config::set_fpath(config_fpath);
	SamplePool spool;
	spool.load_from_conf();
	/*
	for (auto &i: spool.train)
		i.to_image().display(-1);
		*/

	MultipleDigitNN mnn;
	mnn.train(spool);
}

static void run_on_test_set(const char *config_fpath) {
	Config::set_fpath(config_fpath);
	SamplePool spool;
	spool.load_from_conf();

	FILE *fout = fopen(ssprintf("%s/test_result.txt",
			Config::get("system")["output_dir"].as_string().c_str()).c_str(),
			"w");

	MultipleDigitNN network;
	network.load();
	for (auto &i: spool.test)
		fprintf(fout, "%d %d\n", i.get_groundtruth(), network.eval(i));
	fclose(fout);
}

int main(int argc, char **argv) {
	if (argc == 1) {
		fprintf(stderr, "usage: %s <command>\n", argv[0]);
		return -1;
	}

	if (!strcmp(argv[1], "train_multiple")) {
		dassert(argc == 3);
		train_multiple(argv[2]);
	} else if (!strcmp(argv[1], "train_single")) {
		dassert(argc == 3);
		train_single(argv[2]);
	}else if (!strcmp(argv[1], "convert")) {
		dassert(argc == 4);
		convert(argv[2], argv[3]);
	} else if (!strcmp(argv[1], "run_on_test_set")) {
		dassert(argc == 3);
		run_on_test_set(argv[2]);
	}
}

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

