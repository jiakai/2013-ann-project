/*
 * $File: network.cc
 * $Date: Mon Nov 18 18:15:53 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "network.hh"
#include "config.hh"
#include <random>

void NeuralNetwork::eval() {
	for (auto i = m_node.begin() + m_nr_input; i != m_node.end(); i ++)
		i->calc_output();
}

void NeuralNetwork::backpropagation_calc_gradient(
		const num_t *expected_output, double weight) {
	for (auto &i: m_node)
		i.delta_sum = 0;
	for (int i = 0; i < m_nr_output; i ++)
		m_output_start[i].init_err(expected_output[i]);

#ifdef CHECK_DERIVATE
	auto calc_err = [&]() {
		double err = 0;
		for (int i = 0; i < m_nr_output; i ++)
			err += sqr(m_output_start[i].output - expected_output[i]);
		return 0.5 * err;
	};

	constexpr double EPS = 1e-4;
#endif

	for (auto i = m_node.rbegin(); i != m_node.rend(); i ++) {
		auto delta = i->delta_sum * i->output_deriv * weight;
		for (auto &j: i->in) {
			j.from.ptr->delta_sum += j.weight * delta;
			j.gradient += delta * j.from.ptr->output;

#ifdef CHECK_DERIVATE
			j.weight += EPS;
			eval();
			auto err0 = calc_err();
			j.weight -= EPS * 2;
			eval();
			j.weight += EPS;
			auto err1 = calc_err();
			auto der = (err0 - err1) / (EPS * 2);
			log_printf("%.6f %.6f", j.gradient, der);
#endif
		}
	}
#ifdef CHECK_DERIVATE
	exit(0);
#endif
}

double NeuralNetwork::backpropagation_update(double factor) {
	double wsum = 0;
	int cnt = 0;
	factor *= m_param.rate;
	for (auto &i: m_node)
		for (auto &j: i.in) {
			cnt ++;
			auto dw = j.gradient * factor;
			wsum += fabs(dw / j.weight);
			j.weight -= dw;
			j.gradient = 0;
		}
	return wsum / cnt;
}

void NeuralNetwork::finish_init() {
	for (auto &i: m_node)
		for (auto &j: i.in)
			j.from.ptr = m_node.data() + j.from.idx;
	m_output_start = m_node.data() + m_node.size() - m_nr_output;

	m_bias_node.resize(m_node.size() - m_nr_input);
	for (int i = m_nr_input; i < int(m_node.size()); i ++) {
		m_node[i].in.emplace_back(m_bias_node.data() + i - m_nr_input, 0);
	}
	for (auto &i: m_bias_node)
		i.output = 1;

	log_printf("network size: %d nodes", int(m_node.size()));
}

void NeuralNetwork::save(const std::string &fpath) {
	FILE *fout = fopen(fpath.c_str(), "w");
	if (!fout)
		throw HwdrErr("failed to open %s: %m", fpath.c_str());
	fprintf(fout, "%d\n", int(m_node.size()));
	for (auto &i: m_node) {
		fprintf(fout, "%d", int(i.in.size()));
		for (auto &j: i.in)
			fprintf(fout, " %.8e", j.weight);
		fprintf(fout, "\n");
	}
	fclose(fout);
}

void NeuralNetwork::load(const std::string &fpath) {
	FILE *fin = fopen(fpath.c_str(), "r");
	if (!fin)
		throw HwdrErr("failed to open %s: %m", fpath.c_str());
	int n;
	fscanf(fin, "%d", &n);
	dassert(n == int(m_node.size()));
	for (auto &i: m_node) {
		fscanf(fin, "%d", &n);
		dassert(n == int(i.in.size()));
		for (auto &j: i.in)
			fscanf(fin, "%lf", &j.weight);
	}
	dassert(!feof(fin) && !ferror(fin));
	fclose(fin);
}

SingleDigitNN::SingleDigitNN(int target_digit):
	m_target_digit(target_digit)
{
	m_nr_input = PATCH_WIDTH * PATCH_HEIGHT;
	m_node.resize(m_nr_input);

	std::mt19937 rnd;
	std::normal_distribution<> normal_dist(0, WEIGHT_INIT_VARIANCE);

	int ftr_layer_begin = m_node.size();
	for (int r = 0, ri = 0; r + FTR_PATCH_SIZE <= PATCH_HEIGHT;
			r += FTR_SLIDE_STEP, ri ++)
		for (int c = 0, ci = 0; c + FTR_PATCH_SIZE <= PATCH_WIDTH;
				c += FTR_SLIDE_STEP, ci ++) {
			m_node.emplace_back();
			auto &arc = m_node.back().in;
			for (int r1 = r; r1 < r + FTR_PATCH_SIZE; r1 ++)
				for (int c1 = c; c1 < c + FTR_PATCH_SIZE; c1 ++)
					arc.emplace_back(r1 * PATCH_WIDTH + c1, normal_dist(rnd));
		}
	for (int r = 0; r + FTR_HBAR_HEIGHT <= PATCH_HEIGHT; r += FTR_HBAR_SLIDE)  {
		m_node.emplace_back();
		auto &arc = m_node.back().in;
		for (int r1 = r; r1 < r + FTR_HBAR_HEIGHT; r1 ++)
			for (int c1 = 0; c1 < PATCH_WIDTH; c1 ++)
				arc.emplace_back(r1 * PATCH_WIDTH + c1, normal_dist(rnd));
	}

	for (int c = 0; c + FTR_VBAR_WIDTH <= PATCH_WIDTH; c += FTR_VBAR_SLIDE) {
		m_node.emplace_back();
		auto &arc = m_node.back().in;
		for (int r1 = 0; r1 < PATCH_HEIGHT; r1 ++)
			for (int c1 = c; c1 < c + FTR_VBAR_WIDTH; c1 ++)
				arc.emplace_back(r1 * PATCH_WIDTH + c1, normal_dist(rnd));
	}
	int ftr_layer_end = m_node.size();

	{
		m_nr_output = 1;
		m_node.emplace_back();
		auto &arc = m_node.back().in;
		for (int j = ftr_layer_begin; j < ftr_layer_end; j ++)
			arc.emplace_back(j, normal_dist(rnd));
	}

	finish_init();
}

void SingleDigitNN::train(const SamplePool &data) {
	m_param.rate = 0.3;
	log_printf("init vldt mcl: %.6f", calc_misclassify(data.validation));
	double prev_vldt_mse = 1;
	for (int iter = 0; ; iter ++) {
		double weight_delta;
		if (iter > 50) {
			double wsum = 0;
			for (auto &i: data.train)
				wsum += train_data_point(i);
			weight_delta = backpropagation_update(1.0 / wsum);
		} else {
			weight_delta = 0;
			for (auto &i: data.train)
				weight_delta +=
					backpropagation_update(1.0 / train_data_point(i));
			weight_delta /= data.train.size();
		}

		auto train_mse = calc_mse(data.train),
			 train_mcl = calc_misclassify(data.train),
			 vldt_mse = calc_mse(data.validation),
			 vldt_mcl = calc_misclassify(data.validation);
		if (iter > 100) {
			if (vldt_mse > prev_vldt_mse) {
				log_printf("[%d] validation error increased, abort training",
						m_target_digit);
				return;
			}
			prev_vldt_mse = vldt_mse;
		}
		save();
		log_printf("[%d] iter %d: train_mse=%.5f train_mcl=%.5f"
				" vldt_mse=%.5f vldt_mcl=%.5f",
				m_target_digit, iter, train_mse, train_mcl, vldt_mse, vldt_mcl);
		log_printf("weight delta: %.5e", weight_delta);
	}
}

double SingleDigitNN::train_data_point(const Sample &sample) {
	num_t output;

	if (sample.get_groundtruth() == m_target_digit) {
		output = 1;
		eval(sample);
		backpropagation_calc_gradient(&output, 10);
		return 10;
	} else {
		output = 0;
		eval(sample);
		backpropagation_calc_gradient(&output);
		return 1;
	}
}

double SingleDigitNN::calc_mse(const std::vector<Sample> &sample) {
	double sum = 0;
	int cnt = 0;
	for (auto &i: sample) {
		auto err = eval(i);
		if (i.get_groundtruth() == m_target_digit) {
			err = sqr(err - 1) * 10;
			cnt += 10;
		}
		else {
			err *= err;
			cnt ++;
		}
		sum += err;
	}
	return sqrt(sum / cnt);
}

double SingleDigitNN::calc_misclassify(const std::vector<Sample> &sample) {
	int nr_wrong = 0, tot = 0;
	for (auto &i: sample) {
		auto rst = eval(i);
		if (i.get_groundtruth() == m_target_digit) {
			if (rst < 0.5) {
				nr_wrong += 10;
				// Sample(i).to_image().display(-1);
			}
			tot += 10;
		}
		else {
			if (rst >= 0.5)
				nr_wrong ++;
			tot ++;
		}
	}
	return double(nr_wrong) / tot;
}

double SingleDigitNN::eval(const Sample &sample) {
	for (int i = 0; i < m_nr_input; i ++)
		m_node[i].output = sample.data()[i] / 255.0;
	NeuralNetwork::eval();
	return m_output_start[0].output;
}

std::string SingleDigitNN::get_fpath() const {
	return ssprintf("%s/%d.txt",
			Config::get("system")["output_dir"].as_string().c_str(),
			m_target_digit);
}

MultipleDigitNN::MultipleDigitNN() {
	m_nr_input = PATCH_WIDTH * PATCH_HEIGHT;
	m_node.resize(m_nr_input);

	std::mt19937 rnd;
	std::normal_distribution<> normal_dist(0, WEIGHT_INIT_VARIANCE);

	int ftr_layer_begin = m_node.size();
	for (int r = 0, ri = 0; r + FTR_PATCH_SIZE <= PATCH_HEIGHT;
			r += FTR_SLIDE_STEP, ri ++)
		for (int c = 0, ci = 0; c + FTR_PATCH_SIZE <= PATCH_WIDTH;
				c += FTR_SLIDE_STEP, ci ++) {
			m_node.emplace_back();
			auto &arc = m_node.back().in;
			for (int r1 = r; r1 < r + FTR_PATCH_SIZE; r1 ++)
				for (int c1 = c; c1 < c + FTR_PATCH_SIZE; c1 ++)
					arc.emplace_back(r1 * PATCH_WIDTH + c1, normal_dist(rnd));
		}
	for (int r = 0; r + FTR_HBAR_HEIGHT <= PATCH_HEIGHT; r += FTR_HBAR_SLIDE)  {
		m_node.emplace_back();
		auto &arc = m_node.back().in;
		for (int r1 = r; r1 < r + FTR_HBAR_HEIGHT; r1 ++)
			for (int c1 = 0; c1 < PATCH_WIDTH; c1 ++)
				arc.emplace_back(r1 * PATCH_WIDTH + c1, normal_dist(rnd));
	}

	for (int c = 0; c + FTR_VBAR_WIDTH <= PATCH_WIDTH; c += FTR_VBAR_SLIDE) {
		m_node.emplace_back();
		auto &arc = m_node.back().in;
		for (int r1 = 0; r1 < PATCH_HEIGHT; r1 ++)
			for (int c1 = c; c1 < c + FTR_VBAR_WIDTH; c1 ++)
				arc.emplace_back(r1 * PATCH_WIDTH + c1, normal_dist(rnd));
	}
	int ftr_layer_end = m_node.size();

	{
		m_nr_output = 10;
		for (int i = 0; i < 10; i ++) {
			m_node.emplace_back();
			auto &arc = m_node.back().in;
			for (int j = ftr_layer_begin; j < ftr_layer_end; j ++)
				arc.emplace_back(j, normal_dist(rnd));
		}
	}

	finish_init();
}

void MultipleDigitNN::train(const SamplePool &data) {
	log_printf("init vldt mcl: %.6f", calc_misclassify(data.validation));
	const int NR_PREV_VLDT = 5;
	double prev_vldt_mse[NR_PREV_VLDT];
	std::fill_n(prev_vldt_mse, NR_PREV_VLDT, 1);
	int batch = Config::get("train")["batch_update_iter"].as_int();
	for (int iter = 0; ; iter ++) {
		m_param.rate = Config::get("train")["learning_rate"].as_double(0.3);
		double weight_delta;
		int batch_enabled = 0;
		if (batch >= 0 && iter >= batch) {
			for (auto &i: data.train)
				train_data_point(i);
			weight_delta = backpropagation_update(1.0 / data.train.size());
			batch_enabled = 1;
		} else {
			weight_delta = 0;
			for (auto &i: data.train) {
				train_data_point(i);
				weight_delta += backpropagation_update();
			}
			weight_delta /= data.train.size();
		}

		auto train_mse = calc_mse(data.train),
			 train_mcl = calc_misclassify(data.train),
			 vldt_mse = calc_mse(data.validation),
			 vldt_mcl = calc_misclassify(data.validation);
		log_printf("iter %d: b=%d train_mse=%.5f train_mcl=%.5f"
				" vldt_mse=%.5f vldt_mcl=%.5f test_mcl=%.5f",
				iter, batch_enabled,
				train_mse, train_mcl, vldt_mse, vldt_mcl,
				calc_misclassify(data.test));
		if (iter > 100) {
			double avg = 0;
			for (auto &i: prev_vldt_mse)
				avg += i;
			avg /= NR_PREV_VLDT;
			if (vldt_mse > avg) {
				log_printf("validation error increased, abort training (%.6f -> %.6f)",
						avg, vldt_mse);
				return;
			}
		}
		for (int i = 1; i < NR_PREV_VLDT; i ++)
			prev_vldt_mse[i - 1] = prev_vldt_mse[i];
		prev_vldt_mse[NR_PREV_VLDT - 1] = vldt_mse;
		save();
		log_printf("weight delta: %.5e", weight_delta);
	}
}

void MultipleDigitNN::train_data_point(const Sample &sample) {
	num_t output[10] = {0};
	output[sample.get_groundtruth()] = 1;
	eval(sample);
	backpropagation_calc_gradient(output);
}

int MultipleDigitNN::eval(const Sample &sample) {
	for (int i = 0; i < m_nr_input; i ++)
		m_node[i].output = sample.data()[i] / 255.0;
	NeuralNetwork::eval();
	int rst = 0;
	auto conf = m_output_start[0].output;
	for (int i = 1; i < 10; i ++)
		if (m_output_start[i].output > conf) {
			conf = m_output_start[i].output;
			rst = i;
		}
	return rst;
}

std::string MultipleDigitNN::get_fpath() const {
	return ssprintf("%s/mdnn.txt",
			Config::get("system")["output_dir"].as_string().c_str());
}

double MultipleDigitNN::calc_mse(const std::vector<Sample> &sample) {
	double sum = 0;
	for (auto &i: sample) {
		eval(i);
		double err = 0;
		for (int d = 0; d < 10; d ++) {
			auto o = m_output_start[d].output;
			if (d == i.get_groundtruth())
				err += sqr(1 - o);
			else
				err += sqr(o);
		}
		sum += err;
	}
	return sqrt(sum / sample.size());
}

double MultipleDigitNN::calc_misclassify(const std::vector<Sample> &sample) {
	int nr_wrong = 0;
	for (auto &i: sample) {
		if (eval(i) != i.get_groundtruth())
			nr_wrong ++;
	}
	return double(nr_wrong) / sample.size();
}


// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

