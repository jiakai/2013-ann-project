/*
 * $File: image.cc
 * $Date: Mon Nov 18 08:56:02 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "image.hh"
#include "utils.hh"
#include "math.hh"

#ifdef HAVE_OPENCV
#include <opencv2/opencv.hpp>
static void cv_mouse_callback(int ev, int x, int y, int flags, void *cb_) {
	auto cb = static_cast<Image::mouse_callback_t*>(cb_);
	(*cb)(ev, x, y);
}
#endif


void Image::display(int waitkey, const char *title,
		mouse_callback_t mouse_callback) const {
#ifdef HAVE_OPENCV
	cv::Mat img(m_height, m_width, CV_8U);
	for (int i = 0; i < m_height; i ++) {
		auto dest = img.ptr(i);
		auto src = ptr(i);
		for (int j = 0; j < m_width; j ++)
			dest[j] = src[j];
	}
	cv::imshow(title, img);
	if (mouse_callback) {
		cv::setMouseCallback(title, cv_mouse_callback, &mouse_callback);
		if (!waitkey)
			waitkey = 1;
	}
	if (waitkey)
		cv::waitKey(waitkey);
	if (mouse_callback)
		cv::destroyWindow(title);
#else
#error "no display driver available"
#endif
}

void Image::save(const char *fout) const {
#ifdef HAVE_OPENCV
	cv::Mat img(m_height, m_width, CV_8U);
	for (int i = 0; i < m_height; i ++) {
		auto dest = img.ptr(i);
		auto src = ptr(i);
		for (int j = 0; j < m_width; j ++)
			dest[j] = src[j];
	}
	cv::imwrite(fout, img);
#else
#error "no display driver available"
#endif
}

Image Image::load(const char *fpath) {
#ifdef HAVE_OPENCV
	cv::Mat img = cv::imread(fpath, CV_LOAD_IMAGE_GRAYSCALE);
	if (img.empty())
		return Image();
	Image rst(img.cols, img.rows);
	for (int i = 0; i < img.rows; i ++) {
		auto dest = rst.ptr(i);
		auto src = img.ptr(i);
		for (int j = 0; j < img.cols; j ++)
			dest[j] = src[j];
	}
	return rst;
#else
#error "no image input library"
#endif
}

Image Image::downscale(float scale) const {
	dassert(scale > 0 && scale < 1);
	Image rst(iround(m_width * scale), iround(m_height * scale));
	auto rs = 1.f / scale;
	for (int r = 0; r < rst.height(); r ++)
		for (int c = 0; c < rst.width(); c ++) {
			int r0 = ifloor(r * rs),
				c0 = ifloor(c * rs);
			if (r0 >= m_height - 1 || c0 >= m_width - 1)
				continue;
			int r1 = min(ifloor((r + 1) * rs), m_height - 1),
				c1 = min(ifloor((c + 1) * rs), m_width - 1),
				s = 0;
			for (int i = r0; i <= r1; i ++)
				for (int j = c0; j <= c1; j ++)
					s += at(i, j);
			rst.at(r, c) = s / ((r1 - r0 + 1) * (c1 - c0 + 1));
		}
	return rst;
}

void Image::draw_rect(const IRect &rect, int color_) {
	pixel_t color = (pixel_t)(color_ & 0xFF);
	for (int i = rect.y0(); i < rect.y1(); i ++)
		at(i, rect.x0()) = at(i, rect.x1() - 1) = color;
	for (int i = rect.x0(); i < rect.x1(); i ++)
		at(rect.y0(), i) = at(rect.y1() - 1, i) = color;
}

int Image::mouse_ev_left_click() {
#ifdef HAVE_OPENCV
	return CV_EVENT_LBUTTONDOWN;
#endif
}

void Image::flip() {
	for (int i = 0; i < m_height; i ++) {
		auto r = ptr(i);
		for (int j = 0; ; j ++) {
			int ot = m_width - 1 - j;
			if (j >= ot)
				break;
			std::swap(r[j], r[ot]);
		}
	}
}

Image Image::fast_scale_up(float scale) const {
	dassert(scale > 1);
	Image rst(ifloor(float(m_width) * scale), ifloor(float(m_height) * scale));
	float drow_num = 0, drow_next_num = scale;
	for (int i = 0; i < m_height; i ++) {
		auto drow = rst.ptr(ifloor(drow_num));
		auto srow = ptr(i);
		float dcol_s = 0, dcol_t = scale;
		for (int j = 0; j < m_width; j ++) {
			if (j == m_width - 1)
				dcol_t = float(rst.m_width);
			std::fill(drow + ifloor(dcol_s), drow + ifloor(dcol_t), srow[j]);
			dcol_s = dcol_t;
			dcol_t += scale;
		}
		if (i == m_height - 1)
			drow_next_num = float(rst.m_height);

		for (int r = ifloor(drow_num) + 1, rt = ifloor(drow_next_num);
				r < rt; r ++) {
			auto s = drow;
			drow += rst.m_width;
			memcpy(drow, s, sizeof(pixel_t) * rst.m_width);
		}
		drow_num = drow_next_num;
		drow_next_num += scale;
	}

	return rst;
}

Image Image::rotate_quadrant(int n) const {
	n %= 4;
	if (!n)
		return copy();
	if (n == 2) {
		Image dest = copy();
		for(auto p0 = dest.ptr(0), p1 = dest.ptr(m_height - 1, m_width - 1);
				p0 < p1;
				std::swap(*(p0 ++), *(p1 --)));
		return dest;
	}

	Image dest(m_height, m_width);
	int scol_start, s_dy, srow_start, s_dx;
	if (n == 1) {
		scol_start = m_width - 1;
		srow_start = 0;
		s_dy = m_width;
		s_dx = -1;
	} else {
		scol_start = 0;
		srow_start = m_height - 1;
		s_dy = -m_width;
		s_dx = 1;
	}

	int scol = scol_start;
	for (int r = 0; r < dest.m_height; r ++) {
		auto ptr_drow = dest.ptr(r);
		auto ptr_src = ptr(srow_start, scol);
		for (int c = 0; c < dest.m_width; c ++) {
			ptr_drow[c] = *ptr_src;
			ptr_src += s_dy;
		}
		scol += s_dx;
	}

	return dest;
}

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

