/*
 * $File: utils.cc
 * $Date: Sun Oct 13 14:12:48 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "utils.hh"
#include "exc.hh"
#include "config.hh"
#include "math.hh"

#include <signal.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/resource.h>

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cstdarg>
#include <ctime>

#include <mutex>

void __assert_failed__(const char *file, const char *func, int line,
		const char *expr) {

	throw AssertionErr("Assertion `%s' failed (%s@%s:%d)\n",
			expr, func, file, line);
}

void __log_printf__(const char *file, const char *func, int line,
		const char *fmt, ...) {
#define TIME_FMT	"[%s %s@%s:%d]"
	static std::mutex mtx;
	static std::string logfile(Config::get("system")["logfile"].as_string(""));

	static const char *time_fmt = nullptr;
	if (!time_fmt) {
		if (isatty(fileno(stderr)))
			time_fmt = "\033[1;31m" TIME_FMT "\033[0m ";
		else
			time_fmt = TIME_FMT " ";
	}
	time_t cur_time;
	time(&cur_time);
	char timestr[64];
	strftime(timestr, sizeof(timestr), "%H:%M:%S",
			localtime(&cur_time));

	{
		LOCK_GUARD(mtx);
		fprintf(stderr, time_fmt, timestr, func, basename(strdupa(file)), line);

		va_list ap;
		va_start(ap, fmt);
		vfprintf(stderr, fmt, ap);
		va_end(ap);
		fputc('\n', stderr);

		if (!logfile.empty()) {
			FILE *flog = fopen(logfile.c_str(), "a");
			if (!flog) {
				fprintf(stderr, "failed to open log file %s: %m\n",
						logfile.c_str());
				exit(-1);
			}
			fprintf(flog, TIME_FMT " ", timestr, func, basename(strdupa(file)), line);

			va_list ap;
			va_start(ap, fmt);
			vfprintf(flog, fmt, ap);
			va_end(ap);
			fputc('\n', flog);
			fclose(flog);
		}
	}

}

double get_cputime() {
	struct timespec tp;
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &tp);
	return double(tp.tv_sec) + double(tp.tv_nsec) * 1e-9;
}

size_t get_peak_vm() {
	static size_t page_size = sysconf(_SC_PAGESIZE);
	struct rusage usage;
	getrusage(RUSAGE_SELF, &usage);
	return usage.ru_maxrss * page_size;
}

double get_realtime() {
	struct timespec tp;
	clock_gettime(CLOCK_REALTIME_COARSE, &tp);
	return double(tp.tv_sec) + double(tp.tv_nsec) * 1e-9;
}

void ProgressReporter::trigger(int delta) {
	auto cur = get_realtime(), dt = cur - m_prev_time;
	__sync_add_and_fetch(&m_cnt_cur_interval, delta);
	__sync_add_and_fetch(&m_cnt_tot, delta);
	if (dt >= 1) {
		LOCK_GUARD(m_mtx);
		if (m_prev_time < 0)
			dt = 0;
		m_prev_time = cur;
		if (!isatty(fileno(stderr)))
			return;

		double speed = m_cnt_cur_interval / dt;
		if (is_good_r(speed)) {
			if (m_avg_speed < 0)
				m_avg_speed = speed;
			else
				m_avg_speed = m_avg_speed * (1 - AVG_UPDATE) + speed * AVG_UPDATE;
		}
		if (m_target > 0)
			fprintf(stderr, "\r%s: cur=%.2f/sec; avg=%.2lf/sec; %d/%d %d%%   ETA %.1lf[sec] ",
					m_name.c_str(), speed, m_avg_speed,
					m_cnt_tot, m_target, int(m_cnt_tot * 100 / m_target),
					(m_target - m_cnt_tot) / m_avg_speed);
		else
			fprintf(stderr, "\r%s: cur=%.2f/sec; avg=%.2lf/sec; %d tot    ",
					m_name.c_str(), speed, m_avg_speed, m_cnt_tot);
		m_cnt_cur_interval = 0;
		fflush(stderr);
	}
}

void ProgressReporter::done() {
	if (isatty(fileno(stderr)))
		fprintf(stderr, "\n");
}

std::string svsprintf(const char *fmt, va_list ap_orig) {
	int size = 100;     /* Guess we need no more than 100 bytes */
	char *p;

	if ((p = (char*)malloc(size)) == nullptr)
		goto err;

	for (; ;) {
		va_list ap;
		va_copy(ap, ap_orig);
		int n = vsnprintf(p, size, fmt, ap);
		va_end(ap);

		if (n < 0)
			goto err;

		if (n < size) {
			std::string rst(p);
			free(p);
			return rst;
		}

		size = n + 1;

		char *np = (char*)realloc(p, size);
		if (!np) {
			free(p);
			goto err;
		} else 
			p = np;
	}

err:
	fprintf(stderr, "could not allocate memory\n");
	abort();
}


std::string ssprintf(const char *fmt, ...) {
	va_list ap;
	va_start(ap, fmt);
	auto rst = svsprintf(fmt, ap);
	va_end(ap);
	return rst;
}

int get_nr_worker_thread() {
	return Config::get("system")["nr_thread"].
		as_int(int(sysconf(_SC_NPROCESSORS_ONLN)));
}

time_t get_mtime(const char *fpath)  {
	struct stat buf;
	if (stat(fpath, &buf))
		throw HwdrErr("failed to stat %s: %m", fpath);
	return buf.st_mtime;
}

void disable_ctrl_c() {
	signal(SIGINT, SIG_IGN);
}

size_t get_file_size(const char *fpath) {
	struct stat s;
	if (stat(fpath, &s))
		throw HwdrErr("failed to stat %s: %m", fpath);
	return s.st_size;
}

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

