/*
 * $File: sample.cc
 * $Date: Mon Nov 18 08:57:02 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "sample.hh"
#include "globalparam.hh"
#include "config.hh"

#include <vector>

bool Sample::load_from_pnt(FILE *fin, bool perform_thresholding) {
	struct header_t {
		uint16_t len;
		char digit[2];
		uint8_t width, height;
	} __attribute__((packed));

	header_t header;
	if (fread(&header, sizeof(header_t), 1, fin) != 1)
		return false;

	header.len -= sizeof(header_t);

	uint8_t raw_data[header.len];
	if (fread(raw_data, header.len, 1, fin) != 1)
		return false;

	dassert(int(header.width) * header.height / 8 <= int(header.len));

	Image img(header.width, header.height);
	for (int r = 0, p = 0; r < header.height; r ++)
		for (int c = 0; c < header.width; c ++, p ++)
			img.at(r, c) = 255 * ((raw_data[p >> 3] >> (7 - (p & 7))) & 1);

	{
		static int dump_id;
		img.save(ssprintf("/tmp/dump/%d.png", dump_id ++).c_str());
	}

	img = crop(img);
	if (img.empty())
		return false;
	img = img.downscale(min(float(PATCH_WIDTH) / img.width(),
				float(PATCH_HEIGHT) / img.height()));
	if (img.width() != PATCH_WIDTH || img.height() != PATCH_HEIGHT) {
		Image bg(PATCH_WIDTH, PATCH_HEIGHT);
		bg.put_sub(img, (PATCH_HEIGHT - img.height()) / 2,
				(PATCH_WIDTH - img.width()) / 2);
		img = bg;
	}

	if (perform_thresholding) {
		int sum = 0;
		for (auto &i: img)
			sum += i;
		int thresh = ifloor(sum / img.area() * 1.2);
		for (auto &i: img) {
			if (int(i) < thresh)
				i = 0;
			else
				i = 255;
		}
	}

	memcpy(m_img_data, img.ptr(), img.area() * sizeof(pixel_t));
	dassert(header.digit[0] == '#');
	m_groundtruth = header.digit[1] - '0';
	return true;
}

Image Sample::crop(Image &src) {
	static const int DR[8] = {-1, -1, 0, 1, 1, 1, 0, -1},
				 DC[8] = {0, 1, 1, 1, 0, -1, -1, -1};

	bool visited[src.width()][src.height()];
	memset(visited, 0, src.area());

	std::vector<std::pair<int, int>> queue;
	bool not_empty = false;

	for (int r = 0; r < src.height(); r ++)
		for (int c = 0; c < src.width(); c ++)
			if (!visited[r][c] && src.at(r, c)) {
				queue.clear();
				queue.emplace_back(r, c);
				visited[r][c] = true;

				for (size_t head = 0; head < queue.size(); head ++) {
					int r0 = queue[head].first, c0 = queue[head].second;
					for (int i = 0; i < 8; i ++) {
						int r1 = r0 + DR[i],
							c1 = c0 + DC[i];
						if (r1 >= 0 && r1 < src.height() &&
								c1 >= 0 && c1 < src.width() &&
								!visited[r1][c1] && src.at(r1, c1)) {
							visited[r1][c1] = true;
							queue.emplace_back(r1, c1);
						}
					}
				}

				if (int(queue.size()) < STOKE_AREA_THRESH) {
					for (auto &i: queue)
						src.at(i.first, i.second) = 0;
				} else
					not_empty = true;
			}

	if (!not_empty)
		return Image();

	std::vector<bool> row_nonempty(src.height()), col_nonempty(src.width());
	for (int r = 0; r < src.height(); r ++)
		for (int c = 0; c < src.width(); c ++)
			if (src.at(r, c))
				row_nonempty[r] = col_nonempty[c] = true;

	auto find_range = [](const std::vector<bool> &v, int &s, int &t) {
		s = -1; t = -1;
		for (int i = 0; i < int(v.size()); i ++) {
			if (v[i]) {
				if (s == -1)
					s = i;
				t = i;
			}
		}
		t ++;
	};
	int r0, r1, c0, c1;
	find_range(row_nonempty, r0, r1);
	find_range(col_nonempty, c0, c1);

	return src.get_sub(IRect(c0, r0, c1 - c0, r1 - r0));
}

void SamplePool::load_from_conf() {
	auto conf = Config::get("train");
	auto sample_file = conf["sample"].as_string();
	FILE *fin = fopen(sample_file.c_str(), "rb");
	if (!fin)
		throw HwdrErr("failed to open %s: %m", sample_file.c_str());

	auto fsize = get_file_size(sample_file.c_str());
	dassert(fsize % sizeof(Sample) == 0);
	int nr_sample = fsize / sizeof(Sample);

	int nr_vldt = ifloor(nr_sample * conf["validation"].as_double()),
		nr_test = ifloor(nr_sample * conf["test"].as_double()),
		nr_train = nr_sample - nr_vldt - nr_test;
	log_printf("sample pool size: train: %d vldt: %d test: %d",
			nr_train, nr_vldt, nr_test);
	dassert(nr_vldt > 0 && nr_test > 0 && nr_train > 0);

	auto load_comp = [&](std::vector<Sample> &pool, size_t size) {
		pool.resize(size);
		dassert(fread(pool.data(), sizeof(Sample), size, fin) == size);
	};
	load_comp(train, nr_train);
	load_comp(validation, nr_vldt);
	load_comp(test, nr_test);
}

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

