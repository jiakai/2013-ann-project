/*
 * $File: config.cc
 * $Date: Sat Oct 12 22:47:49 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "config.hh"
#include "picojson.h"
#include "math.hh"
#include "utils.hh"

#include <fstream>

Config Config::sm_instance;
std::string Config::sm_fpath;
time_t Config::sm_file_last_modify;

class Config::Impl: public Config::ImplBase {
	public:
		picojson::value m_val;

		Impl(const picojson::value &val):
			m_val(val)
		{ }

		std::shared_ptr<Impl> move_down(const std::string &key) const {
			if (!m_val.contains(key))
				return nullptr;
			auto ret = std::make_shared<Impl>(m_val.get(key));
			auto err = picojson::get_last_error();
			if (!err.empty()) {
				log_printf("error while reading config: %s", err.c_str());
				return nullptr;
			}
			return ret;
		}
};

Config::Impl& Config::impl() {
	return dynamic_cast<Impl&>(*m_impl);
}


const Config::Impl& Config::impl() const {
	return dynamic_cast<const Impl&>(*m_impl);
}

Config Config::get(const std::string &name) {
	if (sm_fpath.empty())
		return Config();
	try {
		auto mtime = get_mtime(sm_fpath.c_str());
		if (!sm_instance.m_impl || mtime != sm_file_last_modify) {
			if (sm_instance.m_impl)
				log_printf("config file %s modified, reload", sm_fpath.c_str());
			std::ifstream fin(sm_fpath.c_str());
			picojson::value v;
			fin >> v;
			auto err = picojson::get_last_error();
			if (!err.empty())
				throw HwdrErr("failed to parse `%s': %s",
						sm_fpath.c_str(), err.c_str());
			sm_instance.m_impl = std::make_shared<Impl>(v);
			sm_file_last_modify = mtime;
		}
		return Config(sm_instance.impl().move_down(name), name);
	} catch (HwdrErr &e) {
		if (sm_instance.m_impl) {
			log_printf("failed to reload config: %s", e.what());
			return Config(sm_instance.impl().move_down(name), name);
		}
		throw e;
	}
}

Config Config::operator[] (const std::string &name) const {
	Config ret(m_impl, m_path + "/" + name);
	if (!m_impl)
		return ret;
	ret.m_impl = impl().move_down(name);
	return ret;
}

std::string Config::as_string() const {
	return get_val<std::string>();
}

int Config::as_int() const {
	return int(get_val<double>());
}

double Config::as_double() const {
	return get_val<double>();
}

std::vector<int> Config::as_int_vec(bool allow_absent) const {
	std::vector<int> ret;
	for (auto &i: as_vec(allow_absent))
		ret.push_back(i.as_int());
	return ret;
}

std::vector<std::string> Config::as_string_vec(bool allow_absent) const {
	std::vector<std::string> ret;
	for (auto &i: as_vec(allow_absent))
		ret.push_back(i.as_string());
	return ret;
}

std::vector<Config> Config::as_vec(bool allow_absent) const {
	std::vector<Config> ret;
	using arr_t = picojson::value::array;
	if (!m_impl || !impl().m_val.is<arr_t>()) {
		if (!m_impl && allow_absent)
			return ret;
		throw NoValErr(m_path.c_str());
	}
	auto arr = impl().m_val.get<arr_t>();
	int num = 0;
	for (auto &i: arr) {
		ret.push_back(Config(std::make_shared<Impl>(i),
				ssprintf("%s/[%d]", m_path.c_str(), num)));
		num ++;
	}
	auto err = picojson::get_last_error();
	if (!err.empty()) {
		log_printf("error while reading config: %s", err.c_str());
		throw NoValErr(m_path.c_str());
	}
	return ret;
}

template<typename T>
T Config::get_val() const {
	if (!m_impl || !impl().m_val.is<T>())
		throw NoValErr(m_path.c_str());
	auto ret = impl().m_val.get<T>();
	auto err = picojson::get_last_error();
	if (!err.empty()) {
		log_printf("error while reading config: %s", err.c_str());
		throw NoValErr(m_path.c_str());
	}
	return ret;
}

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

