#!/usr/bin/env python2
# -*- coding: utf-8 -*-
# $File: raw_tool.py
# $Date: Mon Nov 18 20:15:43 2013 +0800
# $Author: jiakai <jia.kai66@gmail.com>

import cv2

from collections import namedtuple
import struct
import sys
import os.path
import os
import numpy as np

Patch = namedtuple('Patch', ['img', 'truth'])

def load_patch(buf, width, height):
    n = width * height
    if n % 4:
        n += 4 - n % 4
    s = struct.Struct('c' * n + 'i')
    rst = []
    for i in range(0, len(buf), s.size):
        cur = s.unpack_from(buf, i)
        data = ''.join(cur[:width*height])
        img = np.fromstring(data, dtype = 'uint8').reshape(height, width)
        truth = cur[-1]
        cur = Patch(img, truth)
        rst.append(cur)

    print 'loaded {} patches'.format(len(rst))
    return rst

def save_list(fout_dir, lst):
    num = 0
    for i in lst:
        fout_path = os.path.join(fout_dir, '{:02}.png'.format(num))
        cv2.imwrite(fout_path, i.img)
        num += 1



if __name__ == '__main__':
    if len(sys.argv) < 5:
        sys.exit('usage: ' + sys.argv[0] +
                ' <raw file> <width> <height> <test result file> <output dir>')

    width = int(sys.argv[2])
    height = int(sys.argv[3])
    with open(sys.argv[1]) as fin:
        patches = load_patch(fin.read(), width, height)
    with open(sys.argv[4]) as fin:
        test_result = [map(int, i.split()) for i in fin]
    fout_dir = sys.argv[5]

    by_num = [[] for _ in range(10)]
    patches = patches[-4199:]
    assert len(patches) == len(test_result)
    for i, j in zip(patches, test_result):
        assert i.truth == j[0]
        if j[0] != j[1]:
            by_num[j[1]].append((j[0], i))

    for i in range(10):
        cur_fout_dir = os.path.join(fout_dir, str(i))
        os.mkdir(cur_fout_dir)
        for num, img in enumerate(by_num[i]):
            cv2.imwrite(os.path.join(cur_fout_dir, '{}-truth{}.png'.format(
                num, img[0])), img[1].img)
            by_num[i][num] = img[1]

    print 'misclassify:', sum(map(len, by_num))
    lst = []

    black = Patch(np.zeros((height, width), dtype = 'uint8'), -1)

    for i in range(10):
        cur = by_num[i][:10]
        cur.extend([black] * (10 - len(cur)))
        lst.extend(cur)

    save_list(fout_dir, lst)

